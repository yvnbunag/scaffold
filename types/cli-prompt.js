/**
 * @typedef {{ [key in string]: string }} Values
 *
 * @typedef {Object} CLIPrompt
 * @property {Values} Values
 */

 module.exports = {}
