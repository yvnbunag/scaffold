/**
 * Usage
 *
 * node ci/scripts/link-readme-to-gitlab.js <source-readme> <target-readme> <gitlab-project-url> <default branch>
 */

const fs = require('fs/promises')

const defaultArguments = {
  GITLAB_PROJECT_URL: 'https://gitlab.com/ianbunag/scaffold',
  DEFAULT_BRANCH: 'main'
}

link(...process.argv.slice(2))

async function link(
  source = 'README.local.md',
  target = 'README.md',
  gitlabProjectURL = defaultArguments.GITLAB_PROJECT_URL,
  defaultBranch = defaultArguments.DEFAULT_BRANCH
) {
  const contents = await fs.readFile(source, 'utf8')
  const linkedContents = pipe(
    [
      contents,
      linkFile,
      linkMedia,
    ],
    (previousResult, nextMethod) => {
      return nextMethod(previousResult, gitlabProjectURL, defaultBranch)
    }
  )

  await fs.writeFile(target, linkedContents)

  console.log(
    `Generated GitLab project linked markdown '${target}' from '${source}'`
  )
}

/**
 * Pipe initial argument to succeeding methods
 */
function pipe(
  chain = [],
  invoke = (previousResult, nextMethod) => nextMethod(previousResult)
) {
  return chain.reduce((previousResult, nextMethod) => {
    return invoke(previousResult, nextMethod)
  })
}

/**
 * Replace `[<display-name>](./` pattern that does not start with `!` with link
 *  to Gitlab project directory / file
 */
function linkFile(
  contents = '',
  gitlabProjectURL = defaultArguments.GITLAB_PROJECT_URL,
  defaultBranch = defaultArguments.DEFAULT_BRANCH,
) {
  return contents.replace(
    /([^!]\[.*\]\()\.\//g,
    `$1${gitlabProjectURL}/-/tree/${defaultBranch}/`
  )
}

/**
 * Replace `![<display-name>](./` pattern with link to raw Gitlab project
 *  directory / file
 */
function linkMedia(
  contents = '',
  gitlabProjectURL = defaultArguments.GITLAB_PROJECT_URL,
  defaultBranch = defaultArguments.DEFAULT_BRANCH,
) {
  return contents.replace(
    /(!\[.*\]\()\.\//g,
    `$1${gitlabProjectURL}/-/raw/${defaultBranch}/`
  )
}
