class ScaffoldAbortedError extends Error {
  constructor(message = 'Scaffold aborted') {
    super(message)
  }

  static throw() {
    throw new ScaffoldAbortedError()
  }
}

module.exports = {
  ScaffoldAbortedError
}
