/**
 * @type {import('@/template').Definition}
 */
const config = {
  display: "Coding challenge",
  isDomain: true,
}

module.exports = config
