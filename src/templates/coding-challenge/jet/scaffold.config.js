const { standardReplacements } = require('~/config')

/**
 * @type {import('@/template').Definition}
 */
const config = {
  display: 'JET (Jest, ESLint and TypeScript)',
  replacements: standardReplacements,
}

module.exports = config
