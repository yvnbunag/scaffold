/* eslint-disable no-console */
import { subtract } from '~/modules/subtract'

console.log(`10 - 5 = ${subtract(10, 5)}`)
console.log(`(-10) - (-5) = ${subtract(-10, -5)}`)
console.log(`5 - (-5) = ${subtract(5, -5)}`)