/* eslint-disable */
const { standardReplacements } = require('~/config');

/**
 * @type {import('@/template').Definition}
 */
const config = {
  display: 'TVT (TypeORM, Vitest and TypeScript)',
  successMessages: ['Verify package.json before starting development'],
  replacements: standardReplacements,
};

module.exports = config;
