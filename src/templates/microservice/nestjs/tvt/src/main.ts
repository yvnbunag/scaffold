import { AppModule } from '@/app.module';
import { ConfigService } from '@/global/config/config.service';
import { LoggerService } from '@/global/logger/logger.service';
import { NestFactory } from '@nestjs/core';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { bufferLogs: true });
  app.useLogger(app.get(LoggerService));
  await app.listen(app.get(ConfigService).port);
}
bootstrap();
