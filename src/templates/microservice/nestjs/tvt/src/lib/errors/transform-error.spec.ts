import { transformError } from '@/lib/errors/transform-error';
import { describe, expect, it } from 'vitest';

describe('transformError', () => {
  class SourceError extends Error {}
  class TargetError extends Error {}

  it('should transform error on match', () => {
    const sourceError = new SourceError('source');
    const transformedError = transformError(sourceError, [
      [
        SourceError,
        (error) => new TargetError(`${error.message} wrapped in target`),
      ],
    ]);

    expect(transformedError).not.toBe(sourceError);
    expect(transformedError).toBeInstanceOf(TargetError);
    expect(transformedError.message).toEqual('source wrapped in target');
  });

  it('should return original error if there are no matches', () => {
    const sourceError = new Error('source');
    const transformedError = transformError(sourceError, [
      [
        SourceError,
        (error) => new TargetError(`${error.message} wrapped in target`),
      ],
    ]);

    expect(transformedError).toBe(sourceError);
  });
});
