import { ClassConstructor } from 'class-transformer';

export function transformError<CaughtError>(
  error: CaughtError,
  transformations: [ClassConstructor<unknown>, (error: CaughtError) => Error][],
) {
  for (const [ErrorClass, transform] of transformations) {
    if (error instanceof ErrorClass) {
      return transform(error);
    }
  }

  return error;
}
