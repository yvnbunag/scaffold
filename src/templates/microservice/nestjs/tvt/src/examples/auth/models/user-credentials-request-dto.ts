import { IsEmail, IsNotEmpty, IsString } from 'class-validator';

export class UserCredentialsRequestDto {
  @IsEmail()
  email: string;

  @IsNotEmpty()
  @IsString()
  password: string;
}
