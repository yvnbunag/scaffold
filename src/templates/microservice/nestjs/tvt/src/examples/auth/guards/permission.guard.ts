import { JwtUser, Permission } from '@/examples/auth/auth.types';
import {
  CanActivate,
  ExecutionContext,
  Injectable,
  SetMetadata,
  UnauthorizedException,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';

@Injectable()
export class PermissionGuard implements CanActivate {
  private static requiredPermissionKey = Symbol(
    'permission-guard-required-permission-key',
  );

  constructor(private reflector: Reflector) {}

  canActivate(context: ExecutionContext) {
    const requiredPermission = PermissionGuard.getPermission(
      this.reflector,
      context,
    );

    if (!requiredPermission) {
      return true;
    }

    const request = context.switchToHttp().getRequest();
    const user: JwtUser = request.user;

    if (!user || !user.permissions.includes(requiredPermission)) {
      throw new UnauthorizedException('unauthorized');
    }

    return true;
  }

  static require(permission: Permission) {
    return SetMetadata(PermissionGuard.requiredPermissionKey, permission);
  }

  private static getPermission(
    reflector: Reflector,
    context: ExecutionContext,
  ) {
    return reflector.getAllAndOverride<Permission>(
      PermissionGuard.requiredPermissionKey,
      [context.getHandler(), context.getClass()],
    );
  }
}
