import { AuthService } from '@/examples/auth/auth.service';
import { AuthGuard } from '@/examples/auth/guards/auth.guard';
import { NewUserRequestDto } from '@/examples/auth/models/new-user-request-dto';
import { UserCredentialsRequestDto } from '@/examples/auth/models/user-credentials-request-dto';
import { UserTokenResponseDto } from '@/examples/auth/models/user-token-response-dto';
import { UserResponseDto } from '@/examples/users/models/user-response-dto';
import { ResponseDtoInterceptor } from '@/global/response/response-dto.interceptor';
import { transformError } from '@/lib/errors/transform-error';
import {
  Body,
  ConflictException,
  Controller,
  HttpCode,
  HttpStatus,
  Post,
  UnauthorizedException,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';

@Controller('/examples/auth')
@UseGuards(AuthGuard)
export class AuthController {
  constructor(private authService: AuthService) {}

  @AuthGuard.public()
  @Post('/signup')
  @UseInterceptors(ResponseDtoInterceptor.use(UserResponseDto))
  async signup(@Body() newUser: NewUserRequestDto): Promise<UserResponseDto> {
    try {
      return await this.authService.signup(newUser);
    } catch (error) {
      throw transformError(error, [
        [
          AuthService.errors.EmailTakenError,
          (cause) => new ConflictException('email already taken', { cause }),
        ],
      ]);
    }
  }

  @AuthGuard.public()
  @Post('/login')
  @HttpCode(HttpStatus.OK)
  @UseInterceptors(ResponseDtoInterceptor.use(UserTokenResponseDto))
  async authenticateUser(
    @Body() credentials: UserCredentialsRequestDto,
  ): Promise<UserTokenResponseDto> {
    try {
      return { token: await this.authService.authenticateUser(credentials) };
    } catch (error) {
      throw transformError(error, [
        [
          AuthService.errors.UnauthorizedError,
          (cause) => new UnauthorizedException('unauthorized', { cause }),
        ],
      ]);
    }
  }
}
