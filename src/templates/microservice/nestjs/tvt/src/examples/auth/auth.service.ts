import { JwtUser } from '@/examples/auth/auth.types';
import { EmailTakenError } from '@/examples/auth/errors/email-taken-error';
import { UnauthorizedError } from '@/examples/auth/errors/unauthorized-error';
import { UsersService } from '@/examples/users/users.service';
import { transformError } from '@/lib/errors/transform-error';
import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  static errors = {
    EmailTakenError,
    UnauthorizedError,
  };

  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async signup(newUser: { email: string; password: string }) {
    try {
      return await this.usersService.createUser(newUser);
    } catch (error) {
      throw transformError(error, [
        [
          UsersService.errors.EmailTakenError,
          () => new AuthService.errors.EmailTakenError(),
        ],
      ]);
    }
  }

  async authenticateUser(credentials: {
    email: string;
    password: string;
  }): Promise<string> {
    const user = await this.usersService.getUserByCredentials(credentials);

    if (!user) {
      throw new AuthService.errors.UnauthorizedError();
    }

    const payload: JwtUser = {
      userId: user.id,
      email: user.email,
      roles: user.userRoles.map((userRole) => userRole.role.role),
      permissions: user.userPermissions.map(
        (userPermission) => userPermission.permission.permission,
      ),
    };

    return this.jwtService.signAsync(payload);
  }
}
