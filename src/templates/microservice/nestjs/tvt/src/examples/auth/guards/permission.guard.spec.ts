import { PermissionGuard } from '@/examples/auth/guards/permission.guard';
import { Controller, Get, HttpStatus, UseGuards } from '@nestjs/common';
import request from 'supertest';
import { createTestApp } from 'test/lib/create-test-app';
import { createTestJwtUser } from 'test/lib/create-test-jwt-user';
import { RequestInjector } from 'test/lib/request-injector';
import { describe, expect, it } from 'vitest';

describe('PermissionGuard', () => {
  it('should return successful response on unguarded controller and handler', async () => {
    @Controller()
    @UseGuards(PermissionGuard)
    class TestController {
      @Get()
      getHandler() {
        return '';
      }
    }

    const { nestApp } = await createTestApp({ controllers: [TestController] });
    const response = await request(nestApp.getHttpServer()).get('/');

    expect(response.statusCode).toEqual(HttpStatus.OK);
  });

  it('should return unauthorized response on unauthenticated request', async () => {
    @Controller()
    @UseGuards(PermissionGuard)
    class TestController {
      @Get()
      @PermissionGuard.require('get:user')
      getHandler() {
        return '';
      }
    }

    const { nestApp } = await createTestApp({ controllers: [TestController] });
    const response = await request(nestApp.getHttpServer()).get('/');

    expect(response.statusCode).toEqual(HttpStatus.UNAUTHORIZED);
  });

  it('should return successful response on matching handler permission', async () => {
    @Controller()
    @UseGuards(PermissionGuard)
    @RequestInjector.inject(
      'user',
      createTestJwtUser({ permissions: ['get:user'] }),
    )
    class TestController {
      @Get()
      @PermissionGuard.require('get:user')
      getHandler() {
        return '';
      }
    }

    const { nestApp } = await createTestApp({ controllers: [TestController] });
    const response = await request(nestApp.getHttpServer()).get('/');

    expect(response.statusCode).toEqual(HttpStatus.OK);
  });

  it('should return successful response on matching controller permission', async () => {
    @Controller()
    @UseGuards(PermissionGuard)
    @PermissionGuard.require('get:user')
    @RequestInjector.inject(
      'user',
      createTestJwtUser({ permissions: ['get:user'] }),
    )
    class TestController {
      @Get()
      getHandler() {
        return '';
      }
    }

    const { nestApp } = await createTestApp({ controllers: [TestController] });
    const response = await request(nestApp.getHttpServer()).get('/');

    expect(response.statusCode).toEqual(HttpStatus.OK);
  });

  it('should return unauthorized response on unmatched handler permission', async () => {
    @Controller()
    @UseGuards(PermissionGuard)
    @RequestInjector.inject(
      'user',
      createTestJwtUser({ permissions: ['get:note'] }),
    )
    class TestController {
      @Get()
      @PermissionGuard.require('get:user')
      getHandler() {
        return '';
      }
    }

    const { nestApp } = await createTestApp({ controllers: [TestController] });
    const response = await request(nestApp.getHttpServer()).get('/');

    expect(response.statusCode).toEqual(HttpStatus.UNAUTHORIZED);
  });

  it('should return unauthorized response on unmatched controller permission', async () => {
    @Controller()
    @UseGuards(PermissionGuard)
    @PermissionGuard.require('get:user')
    @RequestInjector.inject('user', createTestJwtUser({ permissions: [] }))
    class TestController {
      @Get()
      getHandler() {
        return '';
      }
    }

    const { nestApp } = await createTestApp({ controllers: [TestController] });
    const response = await request(nestApp.getHttpServer()).get('/');

    expect(response.statusCode).toEqual(HttpStatus.UNAUTHORIZED);
  });

  it('should prioritize handler permission over controller permission', async () => {
    @Controller()
    @UseGuards(PermissionGuard)
    @PermissionGuard.require('get:user')
    @RequestInjector.inject(
      'user',
      createTestJwtUser({ permissions: ['get:user'] }),
    )
    class TestController {
      @Get()
      @PermissionGuard.require('get:note')
      getHandler() {
        return '';
      }
    }

    const { nestApp } = await createTestApp({ controllers: [TestController] });
    const response = await request(nestApp.getHttpServer()).get('/');

    expect(response.statusCode).toEqual(HttpStatus.UNAUTHORIZED);
  });
});
