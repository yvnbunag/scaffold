import { JwtUser } from '@/examples/auth/auth.types';
import {
  CanActivate,
  ExecutionContext,
  Injectable,
  InternalServerErrorException,
  SetMetadata,
  UnauthorizedException,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';

export type Resource = keyof Pick<JwtUser, 'userId'>;

@Injectable()
export class ResourcesGuard implements CanActivate {
  private static resourcesKey = Symbol('resource-guard-resources-key');

  constructor(private reflector: Reflector) {}

  canActivate(context: ExecutionContext) {
    const resources = ResourcesGuard.getResources(this.reflector, context);

    if (!resources) {
      return true;
    }

    const request = context.switchToHttp().getRequest();
    const user: JwtUser = request.user;
    const params: Record<string, string> = request.params;

    if (!resources.every((resource) => resource in params)) {
      throw new InternalServerErrorException(
        'required resources are not in path parameters',
      );
    }

    if (!user || !resources.every(ResourcesGuard.isAllowed(user, params))) {
      throw new UnauthorizedException('unauthorized');
    }

    return true;
  }

  static require(resources: [Resource, ...Resource[]]) {
    return SetMetadata(ResourcesGuard.resourcesKey, resources);
  }

  private static getResources(reflector: Reflector, context: ExecutionContext) {
    return reflector.getAllAndOverride<Resource[]>(
      ResourcesGuard.resourcesKey,
      [context.getHandler(), context.getClass()],
    );
  }

  private static isAllowed(
    user: JwtUser,
    params: Record<string, string>,
  ): (resource: Resource) => boolean {
    return (resource) =>
      user.roles.includes('admin') ||
      (resource in params && user[resource].toString() === params[resource]);
  }
}
