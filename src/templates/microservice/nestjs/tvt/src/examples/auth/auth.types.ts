import { ExamplePermission } from '@/global/database/entities/example-permission.entity';
import { ExampleRole } from '@/global/database/entities/example-role.entity';

export type Role = ExampleRole['role'];

export type Permission = ExamplePermission['permission'];

export type JwtUser = {
  userId: number;
  email: string;
  roles: Role[];
  permissions: Permission[];
};
