import { AuthController } from '@/examples/auth/auth.controller';
import { AuthService } from '@/examples/auth/auth.service';
import { AuthGuardStrategy } from '@/examples/auth/guards/auth.guard';
import { UsersModule } from '@/examples/users/users.module';
import { ConfigModule } from '@/global/config/config.module';
import { ConfigService } from '@/global/config/config.service';
import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';

@Module({
  imports: [
    ConfigModule,
    UsersModule,
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        secret: configService.jwt.secret,
        signOptions: { expiresIn: configService.jwt.expiry },
      }),
      inject: [ConfigService],
    }),
  ],
  controllers: [AuthController],
  providers: [AuthService, AuthGuardStrategy],
})
export class AuthModule {}
