import {
  AuthGuard,
  AuthGuardStrategy,
} from '@/examples/auth/guards/auth.guard';
import { ConfigModule } from '@/global/config/config.module';
import { Controller, Get, HttpStatus, UseGuards } from '@nestjs/common';
import request from 'supertest';
import { createTestApp } from 'test/lib/create-test-app';
import { describe, expect, it } from 'vitest';

describe('AuthGuard', () => {
  it('should ignore public controllers', async () => {
    @Controller()
    @UseGuards(AuthGuard)
    @AuthGuard.public()
    class TestController {
      @Get()
      getHandler() {
        return '';
      }
    }

    const { nestApp } = await createTestApp({
      imports: [ConfigModule],
      controllers: [TestController],
      providers: [AuthGuardStrategy],
    });
    const response = await request(nestApp.getHttpServer()).get('/');

    expect(response.statusCode).toEqual(HttpStatus.OK);
  });

  it('should ignore public handlers', async () => {
    @Controller()
    @UseGuards(AuthGuard)
    class TestController {
      @Get()
      @AuthGuard.public()
      getHandler() {
        return '';
      }
    }

    const { nestApp } = await createTestApp({
      imports: [ConfigModule],
      controllers: [TestController],
      providers: [AuthGuardStrategy],
    });
    const response = await request(nestApp.getHttpServer()).get('/');

    expect(response.statusCode).toEqual(HttpStatus.OK);
  });

  it('should guard controllers', async () => {
    @Controller()
    @UseGuards(AuthGuard)
    class TestController {
      @Get()
      getHandler() {
        return '';
      }
    }

    const { nestApp } = await createTestApp({
      controllers: [TestController],
    });
    const response = await request(nestApp.getHttpServer()).get('/');

    expect(response.statusCode).toEqual(HttpStatus.UNAUTHORIZED);
  });

  it('should guard handlers', async () => {
    @Controller()
    class TestController {
      @Get()
      @UseGuards(AuthGuard)
      getHandler() {
        return '';
      }
    }

    const { nestApp } = await createTestApp({
      controllers: [TestController],
    });
    const response = await request(nestApp.getHttpServer()).get('/');

    expect(response.statusCode).toEqual(HttpStatus.UNAUTHORIZED);
  });
});
