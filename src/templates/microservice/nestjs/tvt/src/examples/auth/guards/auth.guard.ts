import { JwtUser } from '@/examples/auth/auth.types';
import { ConfigService } from '@/global/config/config.service';
import {
  ExecutionContext,
  Injectable,
  SetMetadata,
  UnauthorizedException,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { AuthGuard as PassportGuard, PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';

@Injectable()
export class AuthGuard extends PassportGuard('jwt') {
  private static isPublicKey = Symbol('auth-guard-is-public-key');

  constructor(private reflector: Reflector) {
    super();
  }

  canActivate(context: ExecutionContext) {
    if (AuthGuard.isPublic(this.reflector, context)) {
      return true;
    }

    return super.canActivate(context);
  }

  handleRequest(err: any, user: any) {
    if (err || !user) {
      throw new UnauthorizedException('unauthorized');
    }

    return user;
  }

  static public() {
    return SetMetadata(AuthGuard.isPublicKey, true);
  }

  private static isPublic(reflector: Reflector, context: ExecutionContext) {
    return reflector.getAllAndOverride<boolean>(AuthGuard.isPublicKey, [
      context.getHandler(),
      context.getClass(),
    ]);
  }
}

@Injectable()
export class AuthGuardStrategy extends PassportStrategy(Strategy) {
  constructor(private configService: ConfigService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: configService.jwt.secret,
    });
  }

  validate(payload: JwtUser): JwtUser {
    return {
      userId: payload.userId,
      email: payload.email,
      roles: payload.roles,
      permissions: payload.permissions,
    };
  }
}
