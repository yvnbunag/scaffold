import { Exclude, Expose } from 'class-transformer';

@Exclude()
export class NoteResponseDto {
  @Expose()
  id = -1;

  @Expose()
  userId = -1;

  @Expose()
  title = '';

  @Expose()
  contents = '';
}
