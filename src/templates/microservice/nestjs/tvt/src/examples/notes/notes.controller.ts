import { AuthGuard } from '@/examples/auth/guards/auth.guard';
import { PermissionGuard } from '@/examples/auth/guards/permission.guard';
import { ResourcesGuard } from '@/examples/auth/guards/resources.guard';
import { RolesGuard } from '@/examples/auth/guards/roles.guard';
import { CreateNoteRequestDto } from '@/examples/notes/models/create-note-request-dto';
import { CreateNoteRequestParams } from '@/examples/notes/models/create-note-request-params';
import { NoteRequestParams } from '@/examples/notes/models/note-request-params';
import { NoteResponseDto } from '@/examples/notes/models/note-response-dto';
import { NotesRequestParams } from '@/examples/notes/models/notes-request-params';
import { NotesResponseDto } from '@/examples/notes/models/notes-response-dto';
import { PartialUpdateNoteRequestDto } from '@/examples/notes/models/partial-update-note-request-dto';
import { UpdateNoteRequestDto } from '@/examples/notes/models/update-note-request-dto';
import { NotesService } from '@/examples/notes/notes.service';
import { ResponseDtoInterceptor } from '@/global/response/response-dto.interceptor';
import { PaginatedRequestParams } from '@/lib/pagination';
import {
  Body,
  Controller,
  Delete,
  Get,
  NotFoundException,
  Param,
  Patch,
  Post,
  Put,
  Query,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';

@Controller('/examples/users/:userId/notes')
@UseGuards(AuthGuard, RolesGuard, PermissionGuard, ResourcesGuard)
@RolesGuard.require(['user', 'admin'])
export class NotesController {
  constructor(private notesService: NotesService) {}

  @Post('/')
  @UseInterceptors(ResponseDtoInterceptor.use(NoteResponseDto))
  @ResourcesGuard.require(['userId'])
  @PermissionGuard.require('create:note')
  createNote(
    @Param() { userId }: CreateNoteRequestParams,
    @Body() newNote: CreateNoteRequestDto,
  ): Promise<NoteResponseDto> {
    return this.notesService.createNote({
      userId,
      title: newNote.title,
      contents: newNote.contents,
    });
  }

  @Get('/')
  @UseInterceptors(ResponseDtoInterceptor.use(NotesResponseDto))
  @ResourcesGuard.require(['userId'])
  @PermissionGuard.require('get:note')
  getNotes(
    @Param() { userId }: NotesRequestParams,
    @Query() pagination: PaginatedRequestParams,
  ): Promise<NotesResponseDto> {
    return this.notesService.getNotes(userId, pagination);
  }

  @Get('/:noteId')
  @UseInterceptors(ResponseDtoInterceptor.use(NoteResponseDto))
  @ResourcesGuard.require(['userId'])
  @PermissionGuard.require('get:note')
  async getNote(
    @Param() { userId, noteId }: NoteRequestParams,
  ): Promise<NoteResponseDto> {
    const note = await this.notesService.getNote(userId, noteId);

    if (!note) {
      throw new NotFoundException('not found');
    }

    return note;
  }

  @Put('/:noteId')
  @UseInterceptors(ResponseDtoInterceptor.use(NoteResponseDto))
  @ResourcesGuard.require(['userId'])
  @PermissionGuard.require('update:note')
  async updateNote(
    @Param() { userId, noteId }: NoteRequestParams,
    @Body() updatedNote: UpdateNoteRequestDto,
  ): Promise<NoteResponseDto> {
    const note = await this.notesService.getNote(userId, noteId);

    if (!note) {
      throw new NotFoundException('not found');
    }

    return this.notesService.updateNote(updatedNote.assign(note));
  }

  @Patch('/:noteId')
  @UseInterceptors(ResponseDtoInterceptor.use(NoteResponseDto))
  @ResourcesGuard.require(['userId'])
  @PermissionGuard.require('update:note')
  async partialUpdateNote(
    @Param() { userId, noteId }: NoteRequestParams,
    @Body(PartialUpdateNoteRequestDto.RequireOneFieldPipe)
    updatedNote: PartialUpdateNoteRequestDto,
  ): Promise<NoteResponseDto> {
    const note = await this.notesService.getNote(userId, noteId);

    if (!note) {
      throw new NotFoundException('not found');
    }

    return this.notesService.updateNote(updatedNote.assign(note));
  }

  @Delete('/:noteId')
  @UseInterceptors(ResponseDtoInterceptor.use(NoteResponseDto))
  @ResourcesGuard.require(['userId'])
  @PermissionGuard.require('delete:note')
  async deleteNote(
    @Param() { userId, noteId }: NoteRequestParams,
  ): Promise<NoteResponseDto> {
    const note = await this.notesService.softDeleteNote(userId, noteId);

    if (!note) {
      throw new NotFoundException('not found');
    }

    return note;
  }
}
