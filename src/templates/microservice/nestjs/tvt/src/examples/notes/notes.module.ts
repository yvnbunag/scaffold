import { ExampleNote } from '@/global/database/entities/example-note.entity';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NotesController } from './notes.controller';
import { NotesService } from './notes.service';

@Module({
  imports: [TypeOrmModule.forFeature([ExampleNote])],
  providers: [NotesService],
  controllers: [NotesController],
})
export class NotesModule {}
