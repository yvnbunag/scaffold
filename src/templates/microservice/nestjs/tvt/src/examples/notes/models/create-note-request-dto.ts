import { IsString, MaxLength } from 'class-validator';

export class CreateNoteRequestDto {
  @MaxLength(50)
  @IsString()
  title: string;

  @MaxLength(500)
  @IsString()
  contents: string;
}
