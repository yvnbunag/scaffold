import { NoteResponseDto } from '@/examples/notes/models/note-response-dto';
import { PaginatedResponseDto } from '@/lib/pagination';

export class NotesResponseDto extends PaginatedResponseDto(NoteResponseDto) {}
