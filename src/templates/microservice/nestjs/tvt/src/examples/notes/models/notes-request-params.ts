import { Type } from 'class-transformer';
import { IsNumber } from 'class-validator';

export class NotesRequestParams {
  @IsNumber()
  @Type(() => Number)
  userId: number;
}
