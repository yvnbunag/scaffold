import { Type } from 'class-transformer';
import { IsNumber } from 'class-validator';

export class UserRequestParams {
  @IsNumber()
  @Type(() => Number)
  userId: number;
}
