import { RolesService } from '@/examples/users/roles.service';
import { UsersController } from '@/examples/users/users.controller';
import { UsersService } from '@/examples/users/users.service';
import { ExamplePermission } from '@/global/database/entities/example-permission.entity';
import { ExampleRole } from '@/global/database/entities/example-role.entity';
import { ExampleUser } from '@/global/database/entities/example-user.entity';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PermissionsService } from './permissions.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([ExampleUser, ExampleRole, ExamplePermission]),
  ],
  controllers: [UsersController],
  providers: [UsersService, RolesService, PermissionsService],
  exports: [UsersService],
})
export class UsersModule {}
