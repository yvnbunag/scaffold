import { ExamplePermission } from '@/global/database/entities/example-permission.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Repository } from 'typeorm';

@Injectable()
export class PermissionsService {
  constructor(
    @InjectRepository(ExamplePermission)
    private permissionsRepository: Repository<ExamplePermission>,
  ) {}

  getPermissions(permissions: ExamplePermission['permission'][]) {
    return this.permissionsRepository.find({
      where: { permission: In(permissions) },
    });
  }
}
