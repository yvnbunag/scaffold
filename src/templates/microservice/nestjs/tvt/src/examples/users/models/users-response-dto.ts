import { UserResponseDto } from '@/examples/users/models/user-response-dto';
import { PaginatedResponseDto } from '@/lib/pagination';

export class UsersResponseDto extends PaginatedResponseDto(UserResponseDto) {}
