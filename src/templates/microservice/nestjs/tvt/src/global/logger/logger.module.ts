import { ConfigModule } from '@/global/config/config.module';
import { ConfigService } from '@/global/config/config.service';
import { Module } from '@nestjs/common';
import { LoggerModule as PinoLoggerModule } from 'nestjs-pino';
import { LoggerService } from './logger.service';

@Module({
  imports: [
    PinoLoggerModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => {
        return {
          pinoHttp: {
            useLevel: configService.logger.level,
            enabled: configService.logger.enabled,
          },
        };
      },
      inject: [ConfigService],
    }),
  ],
  providers: [LoggerService],
  exports: [LoggerService],
})
export class LoggerModule {}
