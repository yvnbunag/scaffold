import { ExampleNote } from '@/global/database/entities/example-note.entity';
import { ExamplePermission } from '@/global/database/entities/example-permission.entity';
import { ExampleRole } from '@/global/database/entities/example-role.entity';
import { ExampleUserPermission } from '@/global/database/entities/example-user-permission.entity';
import { ExampleUserRole } from '@/global/database/entities/example-user-role.entity';
import { ExampleUser } from '@/global/database/entities/example-user.entity';
import { DataSource } from 'typeorm';

export function createDatasource() {
  return new DataSource({
    type: 'mysql',
    host: process.env.DATABASE_HOST,
    port: Number(process.env.DATABASE_PORT),
    database: process.env.DATABASE_NAME,
    username: process.env.DATABASE_USERNAME,
    password: process.env.DATABASE_PASSWORD,
    entities: [
      ExampleUser,
      ExampleNote,
      ExampleRole,
      ExampleUserRole,
      ExamplePermission,
      ExampleUserPermission,
    ],
    migrations: [`${__dirname}/migrations/*-migration.ts`],
    synchronize: false,
  });
}

export default createDatasource();
