import { ExampleUserPermission } from '@/global/database/entities/example-user-permission.entity';
import { ExampleUserRole } from '@/global/database/entities/example-user-role.entity';
import * as crypto from 'crypto';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  Index,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
@Entity()
export class ExampleUser {
  @PrimaryGeneratedColumn()
  id: number;

  @Index('IDX_example_user_email')
  @Column({ unique: true })
  email: string;

  @Column()
  private salt: string;

  @Column()
  private password: string;

  @OneToMany(() => ExampleUserRole, (userRole) => userRole.user, {
    cascade: true,
  })
  userRoles: ExampleUserRole[];

  @OneToMany(
    () => ExampleUserPermission,
    (userPermission) => userPermission.user,
    { cascade: true },
  )
  userPermissions: ExampleUserPermission[];

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;

  setPassword(password: string): ExampleUser {
    this.salt = crypto.randomBytes(16).toString('hex');
    this.password = ExampleUser.hashPassword(password, this.salt);

    return this;
  }

  comparePassword(password: string) {
    return ExampleUser.hashPassword(password, this.salt) === this.password;
  }

  private static hashPassword(password: string, salt: string): string {
    return crypto.pbkdf2Sync(password, salt, 10000, 64, 'sha512').toString();
  }

  static create({
    email,
    password,
  }: Pick<ExampleUser, 'email'> & { password: string }): ExampleUser {
    const exampleUser = new ExampleUser();

    exampleUser.email = email;
    exampleUser.setPassword(password);

    return exampleUser;
  }
}
