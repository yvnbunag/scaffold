import { MigrationInterface, QueryRunner } from "typeorm";

export class Migration1688643293646 implements MigrationInterface {
    name = 'Migration1688643293646'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`example_user\` ADD UNIQUE INDEX \`IDX_d27e1f15838ef604f592459fdb\` (\`email\`)`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`example_user\` DROP INDEX \`IDX_d27e1f15838ef604f592459fdb\``);
    }

}
