import { ExampleUserPermission } from '@/global/database/entities/example-user-permission.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  Index,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

type PermissionAction = 'get' | 'create' | 'update' | 'delete';

type PermissionResource = 'user' | 'note';

type Permission = `${PermissionAction}:${PermissionResource}`;

@Entity()
export class ExamplePermission {
  @PrimaryGeneratedColumn()
  id: number;

  @Index('IDX_example_permission_permission')
  @Column({ type: 'varchar', length: 15, unique: true })
  permission: Permission;

  @OneToMany(
    () => ExampleUserPermission,
    (userPermission) => userPermission.permission,
    { onDelete: 'RESTRICT' },
  )
  userPermissions: ExampleUserPermission[];

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;

  static create({ permission }: Partial<ExamplePermission>): ExamplePermission {
    const examplePermission = new ExamplePermission();

    examplePermission.permission = permission;

    return examplePermission;
  }
}
