import { ExampleUserRole } from '@/global/database/entities/example-user-role.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  Index,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
@Entity()
export class ExampleRole {
  @PrimaryGeneratedColumn()
  id: number;

  @Index('IDX_example_role_role')
  @Column({ type: 'varchar', length: 15, unique: true })
  role: 'user' | 'admin';

  @OneToMany(() => ExampleUserRole, (userRole) => userRole.role, {
    onDelete: 'RESTRICT',
  })
  userRoles: ExampleUserRole[];

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;

  static create({ role }: Pick<ExampleRole, 'role'>): ExampleRole {
    const exampleRole = new ExampleRole();

    exampleRole.role = role;

    return exampleRole;
  }
}
