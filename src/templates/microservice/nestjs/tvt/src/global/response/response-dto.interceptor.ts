import {
  CallHandler,
  ExecutionContext,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import { ClassConstructor, plainToClass } from 'class-transformer';
import { Observable, map } from 'rxjs';

@Injectable()
export class ResponseDtoInterceptor implements NestInterceptor {
  constructor(private readonly Dto: ClassConstructor<unknown>) {}

  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    return next
      .handle()
      .pipe(
        map((data) =>
          plainToClass(this.Dto, data, { exposeDefaultValues: true }),
        ),
      );
  }

  static use(Dto: ClassConstructor<unknown>) {
    return new ResponseDtoInterceptor(Dto);
  }
}
