import { AppModule } from '@/app.module';
import { HttpStatus, INestApplication } from '@nestjs/common';
import request from 'supertest';
import { createTestAdminLogin } from 'test/lib/create-test-admin-login';
import { createTestApp } from 'test/lib/create-test-app';
import {
  createTestUser,
  createTestUserLogin,
} from 'test/lib/create-test-user-login';
import { TestService } from 'test/lib/test-service';
import { beforeEach, describe, expect, it } from 'vitest';

let app: INestApplication;

beforeEach(async () => {
  const { nestApp, testModule } = await createTestApp({
    imports: [AppModule],
    providers: [TestService],
  });

  await testModule.get<TestService>(TestService).seedDatabase();

  app = nestApp;
});

describe('GET /examples/users', () => {
  it('should get all users', async () => {
    await createTestUser(app, {
      email: 'test+1@example.com',
      password: 'password',
    });
    await createTestUser(app, {
      email: 'test+2@example.com',
      password: 'password',
    });
    const admin = await createTestAdminLogin(app);
    const response = await request(app.getHttpServer())
      .get('/examples/users')
      .set('Authorization', 'Bearer ' + admin.token);

    expect(response.body).toEqual({
      success: true,
      data: {
        previousCursor: null,
        nextCursor: null,
        results: [
          {
            id: 1,
            email: 'admin@example.com',
          },
          {
            id: 2,
            email: 'test+1@example.com',
          },
          {
            id: 3,
            email: 'test+2@example.com',
          },
        ],
      },
    });
    expect(response.statusCode).toBe(HttpStatus.OK);
  });

  it('should be paginateable', async () => {
    await createTestUser(app, {
      email: 'test+1@example.com',
      password: 'password',
    });
    await createTestUser(app, {
      email: 'test+2@example.com',
      password: 'password',
    });
    const admin = await createTestAdminLogin(app);

    const page1Response = await request(app.getHttpServer())
      .get('/examples/users?size=1')
      .set('Authorization', 'Bearer ' + admin.token);
    expect(page1Response.body).toEqual({
      success: true,
      data: {
        previousCursor: null,
        nextCursor: expect.any(String),
        results: [
          {
            id: 1,
            email: 'admin@example.com',
          },
        ],
      },
    });
    expect(page1Response.statusCode).toBe(HttpStatus.OK);

    const page2Response = await request(app.getHttpServer())
      .get(
        `/examples/users?size=1&cursor=${page1Response.body.data.nextCursor}`,
      )
      .set('Authorization', 'Bearer ' + admin.token);
    expect(page2Response.body).toEqual({
      success: true,
      data: {
        previousCursor: page1Response.body.data.nextCursor,
        nextCursor: expect.any(String),
        results: [
          {
            id: 2,
            email: 'test+1@example.com',
          },
        ],
      },
    });
    expect(page2Response.statusCode).toBe(HttpStatus.OK);

    const page3Response = await request(app.getHttpServer())
      .get(
        `/examples/users?size=1&cursor=${page2Response.body.data.nextCursor}`,
      )
      .set('Authorization', 'Bearer ' + admin.token);
    expect(page3Response.body).toEqual({
      success: true,
      data: {
        previousCursor: page2Response.body.data.nextCursor,
        nextCursor: null,
        results: [
          {
            id: 3,
            email: 'test+2@example.com',
          },
        ],
      },
    });
    expect(page3Response.statusCode).toBe(HttpStatus.OK);
  });

  it('should exclude deleted users', async () => {
    const firstUser = await createTestUser(app, {
      email: 'test+1@example.com',
      password: 'password',
    });
    await createTestUser(app, {
      email: 'test+2@example.com',
      password: 'password',
    });
    const admin = await createTestAdminLogin(app);

    const deleteResponse = await request(app.getHttpServer())
      .delete(`/examples/users/${firstUser.id}`)
      .set('Authorization', 'Bearer ' + admin.token);
    expect(deleteResponse.statusCode).toBe(HttpStatus.OK);

    const getAllResponse = await request(app.getHttpServer())
      .get('/examples/users')
      .set('Authorization', 'Bearer ' + admin.token);
    expect(getAllResponse.body).toEqual({
      success: true,
      data: {
        previousCursor: null,
        nextCursor: null,
        results: [
          {
            id: 1,
            email: 'admin@example.com',
          },
          {
            id: 3,
            email: 'test+2@example.com',
          },
        ],
      },
    });
    expect(getAllResponse.statusCode).toBe(HttpStatus.OK);
  });
});

describe('GET /examples/users/:userId', () => {
  it('should get user', async () => {
    const user = await createTestUserLogin(app);
    const response = await request(app.getHttpServer())
      .get(`/examples/users/${user.id}`)
      .set('Authorization', 'Bearer ' + user.token);

    expect(response.body).toEqual({
      success: true,
      data: {
        id: user.id,
        email: user.email,
      },
    });
    expect(response.statusCode).toBe(HttpStatus.OK);
  });

  it('should return not found response if user does not exist', async () => {
    const admin = await createTestAdminLogin(app);
    const response = await request(app.getHttpServer())
      .get('/examples/users/1000')
      .set('Authorization', 'Bearer ' + admin.token);

    expect(response.body).toEqual({
      success: false,
      error: 'not found',
    });
    expect(response.statusCode).toBe(HttpStatus.NOT_FOUND);
  });
});

describe('DELETE /examples/users/:userId', () => {
  it('should delete user', async () => {
    const user = await createTestUserLogin(app);
    const admin = await createTestAdminLogin(app);
    const deleteResponse = await request(app.getHttpServer())
      .delete(`/examples/users/${user.id}`)
      .set('Authorization', 'Bearer ' + admin.token);

    expect(deleteResponse.body).toEqual({
      success: true,
      data: {
        id: user.id,
        email: user.email,
      },
    });
    expect(deleteResponse.statusCode).toBe(HttpStatus.OK);

    const getResponse = await request(app.getHttpServer())
      .get(`/examples/users/${user.id}`)
      .set('Authorization', 'Bearer ' + admin.token);

    expect(getResponse.statusCode).toBe(HttpStatus.NOT_FOUND);
  });

  it('should return not found response if user does not exist', async () => {
    const admin = await createTestAdminLogin(app);
    const response = await request(app.getHttpServer())
      .delete('/examples/users/1000')
      .set('Authorization', 'Bearer ' + admin.token);

    expect(response.body).toEqual({
      success: false,
      error: 'not found',
    });
    expect(response.statusCode).toBe(HttpStatus.NOT_FOUND);
  });
});
