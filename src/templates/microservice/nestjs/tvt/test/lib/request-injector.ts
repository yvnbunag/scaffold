import {
  CanActivate,
  ExecutionContext,
  Injectable,
  UseGuards,
} from '@nestjs/common';

@Injectable()
export class RequestInjector implements CanActivate {
  constructor(private key: string, private value: unknown) {}

  canActivate(context: ExecutionContext) {
    const request = context.switchToHttp().getRequest();

    request[this.key] = this.value;

    return true;
  }

  static inject<Value>(key: string, value: Value) {
    return UseGuards(new RequestInjector(key, value));
  }
}
