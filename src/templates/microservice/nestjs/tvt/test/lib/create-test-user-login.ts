import { UserCredentialsRequestDto } from '@/examples/auth/models/user-credentials-request-dto';
import { UserTokenResponseDto } from '@/examples/auth/models/user-token-response-dto';
import { UserResponseDto } from '@/examples/users/models/user-response-dto';
import { HttpStatus, INestApplication } from '@nestjs/common';
import request from 'supertest';

export type TestUser = UserCredentialsRequestDto & UserResponseDto;

export type TestUserLogin = TestUser & UserTokenResponseDto;

export async function createTestUserLogin(
  app: INestApplication,
  credentials: Partial<UserCredentialsRequestDto> = {},
): Promise<TestUserLogin> {
  const testUser = await createTestUser(app, credentials);
  const response = await request(app.getHttpServer())
    .post('/examples/auth/login')
    .send({
      email: testUser.email,
      password: testUser.password,
    });

  if (response.statusCode !== HttpStatus.OK) {
    throw new Error('failed to login test user');
  }

  return {
    token: response.body.data.token,
    ...testUser,
  };
}

export async function createTestUser(
  app: INestApplication,
  credentials: Partial<UserCredentialsRequestDto> = {},
): Promise<TestUser> {
  const payload: UserCredentialsRequestDto = {
    email: 'test@example.com',
    password: 'password',
    ...credentials,
  };
  const response = await request(app.getHttpServer())
    .post('/examples/auth/signup')
    .send(payload);

  if (response.statusCode !== HttpStatus.CREATED) {
    throw new Error('failed to create test user');
  }

  return {
    password: payload.password,
    ...response.body.data,
  };
}
