import { HttpStatus, INestApplication } from '@nestjs/common';
import request from 'supertest';

type TestAdminLogin = { token: string };

export async function createTestAdminLogin(
  app: INestApplication,
): Promise<TestAdminLogin> {
  const response = await request(app.getHttpServer())
    .post('/examples/auth/login')
    .send({
      email: process.env.ADMIN_USERNAME,
      password: process.env.ADMIN_PASSWORD,
    });

  if (response.statusCode !== HttpStatus.OK) {
    throw new Error('failed to login test admin');
  }

  return {
    token: response.body.data.token,
  };
}
