import { seed } from '@/global/database/database.seed';
import { Injectable } from '@nestjs/common';
import { InjectDataSource } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';

@Injectable()
export class TestService {
  constructor(
    @InjectDataSource()
    private readonly typeOrmDataSource: DataSource,
  ) {}

  seedDatabase() {
    return seed(this.typeOrmDataSource);
  }
}
