import { INestApplication, ModuleMetadata } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';

export async function createTestApp(module: ModuleMetadata): Promise<{
  nestApp: INestApplication;
  testModule: TestingModule;
}> {
  const testModule = await Test.createTestingModule(module).compile();

  return {
    nestApp: await testModule.createNestApplication().init(),
    testModule,
  };
}
