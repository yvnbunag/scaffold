import * as routes from '~/routes'
import * as models from '~/models'
import {
  server,
  basicAuth,
  router,
  sendStatusJSON,
  errorHandler,
  interceptNotFound,
} from '@/fastify'
import { connection } from '@/sequelize'
import { version } from '+/package.json'

import type { FastifyInstance } from 'fastify'
import type { Composites } from '@/fastify/types'
import type { ENV, Dependencies } from '#/server'

type ProcessEnv = NodeJS.ProcessEnv & ENV

/**
 * Default composites
 */
export const defaultComposition: Composites = (() => {
  const errorHandlerComposite = errorHandler.create(
    [
      errorHandler.handleHTTPError,
      errorHandler.handleValidationError,
    ],
    (reply, statusCode, payload) => reply
      .status(statusCode)
      .sendStatusJSON(payload),
  )

  return [
    sendStatusJSON.composite,
    errorHandlerComposite,
    interceptNotFound.composite,
  ]
})()

export function start(env: ProcessEnv): FastifyInstance {
  const composition = [
    ...createComposition(env),
    ...defaultComposition,
  ]
  const dependencies: Dependencies = {
    config: createConfig(env),
    models: createModels(env),
  }
  const app: FastifyInstance = server.create<Dependencies>(
    composition,
    dependencies,
  )

  return app.ready(() => server.serve(app, app.dependencies.config.port))
}

function createComposition(env: ProcessEnv): Composites {
  const {
    BASIC_AUTH_USERNAME = '',
    BASIC_AUTH_PASSWORD = '',
  } = env

  return [
    basicAuth.create(BASIC_AUTH_USERNAME, BASIC_AUTH_PASSWORD),
    router.create(Object.values(routes)),
  ]
}

function createConfig(env: ProcessEnv): Dependencies['config'] {
  return {
    port: +(env.PORT || 3010),
    version,
  }
}

export function createModels(env: ProcessEnv): Dependencies['models'] {
  const { initialize, Engine } = connection
  const { TEST_DATABASE = 'storage/test.sqlite' } = env
  const logging = env.NODE_ENV !== 'production' ? console.log : false // eslint-disable-line no-console

  return models.create({
    test: initialize(Engine.SQLITE, { storage: TEST_DATABASE, logging }),
  })
}
