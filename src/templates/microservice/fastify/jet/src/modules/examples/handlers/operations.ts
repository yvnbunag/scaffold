export function add(...addends: Array<number>): number {
  const [first = 0, second = 0, ...nextRest] = addends
  const result = first + second

  if (!nextRest.length) return result

  return add(result, ...nextRest)
}

export function subtract(...addends: Array<number>): number {
  const [first = 0, second = 0, ...nextRest] = addends
  const result = first - second

  if (!nextRest.length) return result

  return subtract(result, ...nextRest)
}
