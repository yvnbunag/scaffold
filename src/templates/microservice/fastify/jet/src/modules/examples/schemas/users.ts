import { define } from '@/fastify/schema'

export const getNotes = define({
  params: {
    type: 'object',
    required: ['userID'],
    properties: { userID: { type: 'number' } },
  },
  query: {
    type: 'object',
    properties: {
      page: {
        type: 'number',
        minimum: 1,
        default: 1,
      },
      limit: {
        type: 'number',
        minimum: 1,
        default: 10,
      },
    },
  },
} as const)

export const createNote = define({
  params: {
    type: 'object',
    required: ['userID'],
    properties: { userID: { type: 'number' } },
  },
  body: {
    type: 'object',
    required: ['title', 'note'],
    properties: {
      title: {
        type: 'string',
        minLength: 1,
      },
      note: {
        type: 'string',
        minLength: 1,
      },
    },
    additionalProperties: false,
  },
} as const)


export const getNote = define({
  params: {
    type: 'object',
    required: ['userID', 'noteID'],
    properties: {
      userID: { type: 'number' },
      noteID: { type: 'number' },
    },
  },
} as const)

export const updateNote = define({
  params: {
    type: 'object',
    required: ['userID', 'noteID'],
    properties: {
      userID: { type: 'number' },
      noteID: { type: 'number' },
    },
  },
  body: {
    type: 'object',
    minProperties: 1,
    properties: {
      title: {
        type: 'string',
        minLength: 1,
      },
      note: {
        type: 'string',
        minLength: 1,
      },
    },
    additionalProperties: false,
  },
} as const)

export const destroyNote = getNote
