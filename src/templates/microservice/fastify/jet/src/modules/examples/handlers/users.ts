import { NotFoundError } from '@/fastify/errors'

import type { Note } from '~/models/note'

export function createNote(
  Note: Note.Model,
  values: Note.CreationAttributes,
): Promise<Note | never> {
  return Note.create(values)
}

export async function getNote(
  Note: Note.Model,
  userID: Note['userID'],
  noteID: Note['id'],
): Promise<Note | never> {
  const note = await Note.findOne({ where: { id: noteID, userID } })

  if (note) return note

  throw new NotFoundError(`Note not found`)
}

export function getNotes(
  Note: Note.Model,
  userID: Note['userID'],
  { page = 1, limit = 10 } = {},
): Promise<Array<Note>> {
  return Note.findAll({
    where: { userID },
    offset: (page - 1) * limit,
    limit,
  })
}

export async function updateNote(
  Note: Note.Model,
  userID: Note['userID'],
  noteID: Note['id'],
  values: Partial<Omit<Note.Attributes, 'id' | 'userID'>>,
): Promise<Note | never> {
  const note = await getNote(Note, userID, noteID)

  return note.update(values)
}

export async function destroyNote(
  Note: Note.Model,
  userID: Note['userID'],
  noteID: Note['id'],
): AsyncSideEffect<void | never> {
  const note = await getNote(Note, userID, noteID)

  return note.destroy()
}
