import { kebabCase } from 'lodash'
import * as errors from '@/fastify/errors'

type Errors = typeof errors

export const errorMap: Record<
  string, Errors[keyof Errors]
> = Object.fromEntries(
  Object
    .entries(errors)
    .map(([key, error]) => [createMapKey(key as keyof Errors), error]),
)

function createMapKey(key: keyof Errors): string {
  return kebabCase(key)
    .split('-')
    .slice(0, -1)
    .join('-')
}
