import { errorMap } from '~/modules/examples/error-map'
import { define } from '@/fastify/schema'

export const throwError = define({
  params: {
    type: 'object',
    required: ['error'],
    properties: {
      error: {
        type: 'string',
        enum: Object.keys(errorMap),
      },
    },
  },
} as const)
