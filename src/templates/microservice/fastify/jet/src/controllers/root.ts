import type { Controller } from '@/fastify/types'

export const getVersion: Controller = function(request, reply) {
  reply.sendStatusJSON(this.dependencies.config.version)
}
