import { basicAuth as handlers } from '~/modules/examples/handlers'

import type { basicAuth as schemas } from '~/modules/examples/schemas'
import type { Controller } from '@/fastify/types'

export const create: Controller<
  typeof schemas.create
> = function (request, reply) {
  const { username, password } = request.body
  const authentication = handlers.create(username, password)

  reply.sendStatusJSON(authentication)
}

export const check: Controller = function (request, reply) {
  reply.sendStatusJSON('authenticated')
}
