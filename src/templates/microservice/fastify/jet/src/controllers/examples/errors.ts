import { errors as handlers } from '~/modules/examples/handlers'

import type { errors as schemas } from '~/modules/examples/schemas'
import type { Controller } from '@/fastify/types'

export const getAll: Controller = function (request, reply) {
  const errors = handlers.getAll()

  reply.sendStatusJSON(errors)
}

export const throwError: Controller<
  typeof schemas.throwError
> = function (request) {
  handlers.throwHTTPError(request.params.error)
}
