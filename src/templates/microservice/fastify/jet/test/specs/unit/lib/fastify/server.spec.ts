import { serve, createDependencyInjection } from '@/fastify/server'
import { getFirstArguments } from '$/utils/mocks'

import type { FastifyInstance } from 'fastify'


describe('serve', () => {
  function createMockListen(error?: Error) {
    return (
      port: number,
      callback: (error: Error | undefined, address: string)=> void,
    ): SideEffect => {
      callback(error, `http://localhost:${port}`)
    }
  }

  const listen = jest.fn()
  const app = { listen } as unknown as FastifyInstance
  const dependencies = {
    process: { exit: jest.fn() },
    console: { log: jest.fn(), error: jest.fn() },
  }

  beforeEach(jest.clearAllMocks)

  it('should serve fastify instance', () => {
    const { log } = dependencies.console

    listen.mockImplementationOnce(createMockListen())

    serve(app, 3010, dependencies as unknown as Parameters<typeof serve>[2])

    expect(log).toHaveBeenCalledTimes(1)
    expect(getFirstArguments(log)).toMatchSnapshot()
  })

  it('should exit on error', () => {
    const { error } = dependencies.console

    listen.mockImplementationOnce(createMockListen(new Error('listen-error')))

    serve(app, 3010, dependencies as unknown as Parameters<typeof serve>[2])

    expect(error).toHaveBeenCalledTimes(1)
    expect(getFirstArguments(error)).toMatchSnapshot()
    expect(dependencies.process.exit).toHaveBeenCalledTimes(1)
  })
})

describe('createDependencyInjection', () => {
  const dependency = { key: 'value' }

  describe('Injection', () => {
    describe('should not inject non objects', () => {
      test.each([
        ['string', 'string'],
        ['number', 1],
        ['symbol', Symbol()],
      ])('%s', (type, value) => {
        expect(createDependencyInjection(value)).toBe(value)
      })
    })

    it('should inject object', () => {
      const injected = createDependencyInjection(dependency)

      expect(injected).not.toBe(dependency)
      expect(Object.keys(dependency)).toEqual(Object.keys(injected))
    })

    it('should not re-inject already injected object', () => {
      const injected = createDependencyInjection(dependency)

      expect(createDependencyInjection(injected)).toBe(injected)
    })
  })

  describe('Fastify decorate checkers', () => {
    it('should return value when accessing defined getter', () => {
      const injection = createDependencyInjection({ getter: 'getter' })

      expect(injection.getter).toMatchInlineSnapshot(`"getter"`)
    })

    it('should return value when accessing defined setter', () => {
      const injection = createDependencyInjection({ setter: 'setter' })

      expect(injection.setter).toMatchInlineSnapshot(`"setter"`)
    })

    it('should return undefined when accessing undefined getter', () => {
      const injection = createDependencyInjection()

      // @ts-expect-error key not injected
      expect(injection.getter).toBeUndefined()
    })

    it('should return undefined when accessing undefined setter', () => {
      const injection = createDependencyInjection()

      // @ts-expect-error key not injected
      expect(injection.setter).toBeUndefined()
    })
  })

  describe('Access', () => {
    it('should return injected property', () => {
      const injected = createDependencyInjection(dependency)

      expect(injected.key).toBe(dependency.key)
    })

    it('should throw error on non injected property access', () => {
      const injected = createDependencyInjection(dependency)

      // @ts-expect-error key not injected
      expect(() => injected['unknown-key']).toThrowErrorMatchingSnapshot()
    })
  })

  describe('Mutation', () => {
    it('should throw error on property mutation', () => {
      const injected = createDependencyInjection(dependency)

      expect(() => injected.key = 'value').toThrowErrorMatchingSnapshot()
    })
  })
})
