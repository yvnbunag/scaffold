import { defaultReplyWithError } from '@/fastify/error-handler'
import { getFirstArguments } from '$/utils/mocks'

import type { FastifyReply } from 'fastify'


describe('defaultReplyWithError', () => {
  it('should reply with status code and payload', () => {
    const reply = {
      status: jest.fn().mockReturnThis(),
      send: jest.fn().mockReturnThis(),
    }

    defaultReplyWithError(
      reply as unknown as FastifyReply,
      500,
      { error:'Error', message: 'error' },
    )

    expect(reply.status).toHaveBeenCalledTimes(1)
    expect(getFirstArguments(reply.status)).toMatchSnapshot()
    expect(reply.send).toHaveBeenCalledTimes(1)
    expect(getFirstArguments(reply.send)).toMatchSnapshot()
  })
})
