import { pick } from 'lodash'
import { defaultComposition } from '~/server'
import { examples } from '~/routes'
import { errorMap } from '~/modules/examples/error-map'
import { server, router, basicAuth } from '@/fastify'
import { createDependencies } from '$/specs/integration/__lib__'
import * as lib from '$/specs/integration/__lib__/examples'
import { createCounter } from '$/utils/generators'

import type { Awaited } from 'ts-essentials'
import type { Dependencies } from '#/server'

const composition = [
  basicAuth.create('test', 'test'),
  router.create([examples]),
  ...defaultComposition,
]

describe('Examples API', () => {
  describe('Operations', () => {
    const app = server.create<Dependencies>(composition)

    afterAll(async () => await app.close())

    describe('POST /examples/operations/add', () => {
      function add(values: Array<number | string>) {
        return app.inject({
          method: 'POST',
          url: '/examples/operations/add',
          payload: { values },
        })
      }

      describe('Successful cases', () => {
        it('should return 0 for empty values', async () => {
          const response = await add([])
          const { success, data } = response.json()

          expect(success).toBe(true)
          expect(data).toMatchInlineSnapshot(`0`)
          expect(response.statusCode).toMatchInlineSnapshot(`200`)
        })

        it('should return single number', async () => {
          const response = await add([1])
          const { success, data } = response.json()

          expect(success).toBe(true)
          expect(data).toMatchInlineSnapshot(`1`)
          expect(response.statusCode).toMatchInlineSnapshot(`200`)
        })

        it('should add two numbers', async () => {
          const response = await add([1, 2])
          const { success, data } = response.json()

          expect(response.statusCode).toMatchInlineSnapshot(`200`)
          expect(success).toBe(true)
          expect(data).toMatchInlineSnapshot(`3`)
        })

        it('should add more than two numbers', async () => {
          const response = await add([1, 2, 3, 4, 5])
          const { success, data } = response.json()

          expect(success).toBe(true)
          expect(data).toMatchInlineSnapshot(`15`)
          expect(response.statusCode).toMatchInlineSnapshot(`200`)
        })

        it('should add numeric string values', async () => {
          const response = await add([1, 2, 3, '4', '5'])
          const { success, data } = response.json()

          expect(success).toBe(true)
          expect(data).toMatchInlineSnapshot(`15`)
          expect(response.statusCode).toMatchInlineSnapshot(`200`)
        })
      })

      describe('Failed cases', () => {
        it('should return error for non numeric value', async () => {
          const response = await add([1, 'a'])

          expect(response.json()).toMatchSnapshot()
          expect(response.statusCode).toMatchInlineSnapshot(`400`)
        })
      })
    })

    describe('POST /examples/operations/subtract', () => {
      function subtract(values: Array<string | number>) {
        return app.inject({
          method: 'POST',
          url: '/examples/operations/subtract',
          payload: { values },
        })
      }

      describe('Successful cases', () => {
        it('should return 0 for empty values', async () => {
          const response = await subtract([])
          const { success, data } = response.json()

          expect(success).toBe(true)
          expect(data).toMatchInlineSnapshot(`0`)
          expect(response.statusCode).toMatchInlineSnapshot(`200`)
        })

        it('should return single number', async () => {
          const response = await subtract([10])
          const { success, data } = response.json()

          expect(success).toBe(true)
          expect(data).toMatchInlineSnapshot(`10`)
          expect(response.statusCode).toMatchInlineSnapshot(`200`)
        })

        it('should subtract two numbers', async () => {
          const response = await subtract([10, 5])
          const { success, data } = response.json()

          expect(success).toBe(true)
          expect(data).toMatchInlineSnapshot(`5`)
          expect(response.statusCode).toMatchInlineSnapshot(`200`)
        })

        it('should subtract more than two numbers', async () => {
          const response = await subtract([10, 5, -4])
          const { success, data } = response.json()

          expect(success).toBe(true)
          expect(data).toMatchInlineSnapshot(`9`)
          expect(response.statusCode).toMatchInlineSnapshot(`200`)
        })

        it('should subtract numeric string values', async () => {
          const response = await subtract(['-10', '-5'])
          const { success, data } = response.json()

          expect(success).toBe(true)
          expect(data).toMatchInlineSnapshot(`-5`)
          expect(response.statusCode).toMatchInlineSnapshot(`200`)
        })
      })

      describe('Failed cases', () => {
        it('should return error for non numeric value', async () => {
          const response = await subtract([10, 'non-numeric'])

          expect(response.json()).toMatchSnapshot()
          expect(response.statusCode).toMatchInlineSnapshot(`400`)
        })
      })
    })
  })

  describe('Basic authentication', () => {
    const app = server.create<Dependencies>(composition)

    afterAll(async () => await app.close())

    function createBasicAuth(username?: string, password?: string) {
      return app.inject({
        method: 'POST',
        url: '/examples/basic-auth/create',
        payload: { username, password },
      })
    }

    describe('POST /examples/basic-auth/create', () => {
      describe('Successful cases', () => {
        it('should create basic auth', async () => {
          const response = await createBasicAuth('test', 'test')
          const { success, data } = response.json()

          expect(success).toBe(true)
          expect(data).toMatchInlineSnapshot(`"Basic dGVzdDp0ZXN0"`)
          expect(response.statusCode).toMatchInlineSnapshot(`200`)
        })
      })

      describe('Failed cases', () => {
        it('should return error if credentials are not provided', async () => {
          const response = await createBasicAuth()

          expect(response.json()).toMatchSnapshot()
          expect(response.statusCode).toMatchInlineSnapshot(`400`)
        })

        it('should return error if username is not provided', async () => {
          const response = await createBasicAuth(undefined, 'test')

          expect(response.json()).toMatchSnapshot()
          expect(response.statusCode).toMatchInlineSnapshot(`400`)
        })

        it('should return error if password is not provided', async () => {
          const response = await createBasicAuth('test')

          expect(response.json()).toMatchSnapshot()
          expect(response.statusCode).toMatchInlineSnapshot(`400`)
        })
      })
    })

    describe('POST /examples/basic-auth/check', () => {
      function checkBasicAuth(authorization: string) {
        return app.inject({
          method: 'POST',
          url: '/examples/basic-auth/check',
          headers: { Authorization: authorization },
        })
      }

      it('should succeed if authorization is valid', async () => {
        const { data } = (await createBasicAuth('test', 'test')).json()
        const response = await checkBasicAuth(data)

        expect(response.json()).toMatchSnapshot()
        expect(response.statusCode).toMatchInlineSnapshot(`200`)
      })

      it('should fail if authorization is invalid', async () => {
        const { data } = (await createBasicAuth('invalid', 'invalid')).json()
        const response = await checkBasicAuth(data)

        expect(response.json()).toMatchSnapshot()
        expect(response.statusCode).toMatchInlineSnapshot(`401`)
      })
    })
  })

  describe('Errors', () => {
    const app = server.create<Dependencies>(composition)

    afterAll(async () => await app.close())

    describe('GET /examples/errors', () => {
      it('should get list of errors', async () => {
        const response = await app.inject({
          method: 'GET',
          url: '/examples/errors',
        })

        expect(response.json()).toMatchSnapshot()
        expect(response.statusCode).toMatchInlineSnapshot(`200`)
      })
    })

    describe('GET /examples/errors/:error', () => {
      const errors: Array<keyof typeof errorMap> = Object.keys(errorMap)

      function throwError(error: typeof errors[number]) {
        return app.inject({
          method: 'GET',
          url: `/examples/errors/${error}`,
        })
      }

      describe('It should return error for', () => {
        test.each(errors)('%s', async (error) => {
          const response = await throwError(error)

          expect(response.json()).toMatchSnapshot()
          expect(response.statusCode).toMatchSnapshot()
        })
      })

      it('should return error if error does not exist', async () => {
        const response = await throwError('unknown-error')

        expect(response.json()).toMatchSnapshot()
        expect(response.statusCode).toMatchInlineSnapshot(`400`)
      })
    })
  })

  describe('Notes', () => {
    const { notes } = lib.users
    const { default: defaultMatcher } = notes.matchers
    const { getAll, create, get, update, destroy } = notes.helpers
    const generateUserID = createCounter(1)
    const userIDs = {
      getAll: {
        MAIN: generateUserID(),
        ALTERNATE: generateUserID(),
        NO_NOTE: generateUserID(),
      },
      create: { MAIN: generateUserID(), ALTERNATE: generateUserID() },
      get: { MAIN: generateUserID(), ALTERNATE: generateUserID() },
      update: {
        success: { MAIN: generateUserID(), ALTERNATE: generateUserID() },
        fail: { MAIN: generateUserID(), ALTERNATE: generateUserID() },
      },
      delete: { MAIN: generateUserID(), ALTERNATE: generateUserID() },
    }
    const defaultValues = {
      title: 'Default title',
      note: 'Default note',
    }
    let app: ReturnType<typeof server.create>
    let models: Awaited<ReturnType<typeof createDependencies>>['models']

    beforeAll(async () => {
      const dependencies = await createDependencies()

      models = dependencies.models
      app = server.create<Dependencies>(composition, dependencies)
    })
    afterAll(async () => await app.close())

    describe('GET /users/:userID/notes', () => {
      const userID = userIDs.getAll.MAIN
      const seed = 'Get All'
      const fixtures = notes.fixtures.createFromArray([
        [{ userID }, { from: 1, to: 5, seed }],
        [
          { userID, deletedAt: new Date() },
          { from: 6, to: 7, seed },
        ],
        [{ userID: userIDs.getAll.ALTERNATE }, { from: 8, to: 10, seed }],
        [{ userID }, { from: 11, to: 20, seed }],
      ])

      beforeAll(async () => {
        await notes.fixtures.load(fixtures, models.getAll())
      })

      describe('Successful cases', () => {
        it('should return first 10 notes by default', async () => {
          const response = await getAll(app, userID)
          const payload = response.json()
          const { length } = payload.data

          expect(length).toBe(10)
          expect(payload).toMatchSnapshot({
            data: new Array(length).fill(defaultMatcher),
          })
          expect(response.statusCode).toMatchInlineSnapshot(`200`)
        })

        it('should be paginate-able', async () => {
          const response = await getAll(app, userID, { page: '2' })
          const payload = response.json()

          expect(payload).toMatchSnapshot({
            data: new Array(payload.data.length).fill(defaultMatcher),
          })
          expect(response.statusCode).toMatchInlineSnapshot(`200`)
        })

        it('should be limit-able', async () => {
          const response = await getAll(app, userID, { limit: '5' })
          const payload = response.json()

          expect(payload).toMatchSnapshot({
            data: new Array(payload.data.length).fill(defaultMatcher),
          })
          expect(response.statusCode).toMatchInlineSnapshot(`200`)
        })

        it('should be paginate-able and limit-able', async () => {
          const response = await getAll(app, userID, { page: '2', limit: '5' })
          const payload = response.json()

          expect(payload).toMatchSnapshot({
            data: new Array(payload.data.length).fill(defaultMatcher),
          })
          expect(response.statusCode).toMatchInlineSnapshot(`200`)
        })

        it('should return empty array if user has no notes', async () => {
          const response = await getAll(app, userIDs.getAll.NO_NOTE)
          const payload = response.json()

          expect(payload).toMatchSnapshot()
          expect(response.statusCode).toMatchInlineSnapshot(`200`)
        })

        it('should return empty array when notes pagination is exhausted', async () => {
          const response = await getAll(app, userID, { page: '1000' })
          const payload = response.json()

          expect(payload).toMatchSnapshot()
          expect(response.statusCode).toMatchInlineSnapshot(`200`)
        })
      })

      describe('Failed cases', () => {
        it('should return error if page is invalid', async () => {
          const response = await getAll(app, userID, { page: 'invalid' })
          const payload = response.json()

          expect(payload).toMatchSnapshot()
          expect(response.statusCode).toMatchInlineSnapshot(`400`)
        })

        it('should return error if limit is invalid', async () => {
          const response = await getAll(app, userID, { limit: 'invalid' })
          const payload = response.json()

          expect(payload).toMatchSnapshot()
          expect(response.statusCode).toMatchInlineSnapshot(`400`)
        })
      })
    })

    describe('POST /users/:userID/notes', () => {
      const userID = userIDs.create.MAIN
      const title = 'Created title'
      const note = 'Created note'

      describe('Successful cases', () => {
        it('should create note', async () => {
          const createResponse = await create(app, userID, { title, note })
          const createPayload = createResponse.json()
          const getResponse = await get(app, userID, createPayload.data.id)

          expect(createPayload).toMatchSnapshot({ data: defaultMatcher })
          expect(createResponse.statusCode).toMatchInlineSnapshot(`200`)
          expect(getResponse.statusCode).toMatchInlineSnapshot(`200`)
        })

        it('should create note with additional properties ignored', async () => {
          const data = { title, note, userID: userIDs.create.ALTERNATE }
          const response = await create(app, userID, data)
          const payload = response.json()

          expect(payload).toMatchSnapshot({ data: defaultMatcher })
          expect(response.statusCode).toMatchInlineSnapshot(`200`)
        })
      })

      describe('Failed cases', () => {
        const cases: Array<[string, Record<string, unknown>?]> = [
          ['title is not provided', { note }],
          ['title is empty', { title: '', note }],
          ['title is invalid', { title: [], note }],
          ['note is not provided', { title }],
          ['note is empty', { title, note: '' }],
          ['note is invalid', { title, note: [] }],
          ['no property is provided', {}],
          ['payload is not provided', undefined],
        ]

        describe('It should return error if', () => {
          test.each(cases)('%s', async (description, data) => {
            const response = await create(app, userID, data)
            const payload = response.json()

            expect(payload).toMatchSnapshot()
            expect(response.statusCode).toMatchSnapshot()
          })
        })
      })
    })

    describe('GET /users/:userID/notes/:noteID', () => {
      const userID = userIDs.get.MAIN
      const seed = 'Get'
      let note: Awaited<ReturnType<typeof notes.fixtures.load>>[number]

      beforeAll(async () => {
        const fixtures = notes.fixtures.create(
          { userID, ...defaultValues },
          { seed },
        )
        note = (await notes.fixtures.load(fixtures, models.getAll()))[0]
      })

      describe('Successful cases', () => {
        it('should get note', async () => {
          const response = await get(app, note.userID, note.id)
          const payload = response.json()

          expect(payload).toMatchSnapshot({ data: defaultMatcher })
          expect(response.statusCode).toMatchInlineSnapshot(`200`)
        })
      })

      describe('Failed cases', () => {
        it('should return error if note does not exist', async () => {
          const response = await get(app, userID, 1000)
          const payload = response.json()

          expect(payload).toMatchSnapshot()
          expect(response.statusCode).toMatchInlineSnapshot(`404`)
        })

        it('should return error if note is not owned by user', async () => {
          const response = await get(app, userIDs.get.ALTERNATE, note.id)
          const payload = response.json()

          expect(payload).toMatchSnapshot()
          expect(response.statusCode).toMatchInlineSnapshot(`404`)
        })
      })
    })

    describe('PATCH /users/:userID/notes/:noteID', () => {
      const seed = 'Update'
      const updatedValues = {
        title: 'Updated title',
        note: 'Updated note',
      }

      describe('Successful cases', () => {
        const userID = userIDs.update.success.MAIN

        describe(`It should update note's`, () => {
          const cases: Array<[string, Record<string, unknown>]> = [
            ['title', { title: updatedValues.title }],
            ['note', { note: updatedValues.note }],
            ['title and note', updatedValues],
            [
              'title and note with additional properties ignored',
              { ...updatedValues, userID: userIDs.update.success.ALTERNATE },
            ],
          ]

          test.each(cases)('%s', async (description, data) => {
            const fixtures = notes.fixtures.create(
              { userID, ...defaultValues },
              { seed },
            )
            const [note] = await notes.fixtures.load(fixtures, models.getAll())
            const response = await update(app, note.userID, note.id, data)
            const payload = response.json()
            const updateKeys = Object.keys(data)
            const [initialValues, updatedValues] = [note, payload.data].map(
              (values) => pick(values, updateKeys),
            )

            expect(initialValues).not.toEqual(updatedValues)
            expect(payload).toMatchSnapshot({ data: defaultMatcher })
            expect(response.statusCode).toMatchSnapshot()
          })
        })
      })

      describe('Failed cases', () => {
        const userID = userIDs.update.fail.MAIN
        let note: Awaited<ReturnType<typeof notes.fixtures.load>>[number]

        beforeAll(async () => {
          const fixtures = notes.fixtures.create(
            { userID, ...defaultValues },
            { seed },
          )
          note = (await notes.fixtures.load(fixtures, models.getAll()))[0]
        })

        describe('It should return error if', () => {
          const cases: Array<[string, Record<string, unknown>?]> = [
            ['title is empty', { title: '' }],
            ['title is invalid', { title: [] }],
            ['note is empty', { note: '' }],
            ['note is invalid', { note: [] }],
            ['no property is provided', {}],
            ['payload is not provided', undefined],
          ]

          test.each(cases)('%s', async (description, data) => {
            const response = await update(app, note.userID, note.id, data)
            const payload = response.json()

            expect(payload).toMatchSnapshot()
            expect(response.statusCode).toMatchSnapshot()
          })
        })

        it('should return error if note does not exist', async () => {
          const response = await update(app, userID, 1000, updatedValues)
          const payload = response.json()

          expect(payload).toMatchSnapshot()
          expect(response.statusCode).toMatchInlineSnapshot(`404`)
        })

        it('should return error if note is not owned by user', async () => {
          const response = await update(
            app,
            userIDs.update.fail.ALTERNATE,
            note.id,
            updatedValues,
          )
          const payload = response.json()

          expect(payload).toMatchSnapshot()
          expect(response.statusCode).toMatchInlineSnapshot(`404`)
        })
      })
    })

    describe('DELETE /users/:userID/notes/:noteID', () => {
      const userID = userIDs.delete.MAIN
      const seed = 'Delete'

      describe('Successful cases', () => {
        it('should delete note', async () => {
          const fixtures = notes.fixtures.create(
            { userID, ...defaultValues },
            { seed },
          )
          const [note] = await notes.fixtures.load(fixtures, models.getAll())
          const deleteResponse = await destroy(app, userID, note.id)
          const deletePayload = deleteResponse.json()
          const getResponse = await get(app, userID, note.id)
          const getPayload = getResponse.json()

          expect(deletePayload).toMatchSnapshot()
          expect(deleteResponse.statusCode).toMatchInlineSnapshot(`200`)
          expect(getPayload).toMatchSnapshot()
        })
      })

      describe('Failed cases', () => {
        it('should return error if note does not exist', async () => {
          const response = await destroy(app, userID, 1000)
          const payload = response.json()

          expect(payload).toMatchSnapshot()
          expect(response.statusCode).toMatchInlineSnapshot(`404`)
        })

        it('should return error if note is not owned by user', async () => {
          const fixtures = notes.fixtures.create(
            { userID, ...defaultValues },
            { seed },
          )
          const [note] = await notes.fixtures.load(fixtures, models.getAll())
          const response = await destroy(app, userIDs.delete.ALTERNATE, note.id)
          const payload = response.json()

          expect(payload).toMatchSnapshot()
          expect(response.statusCode).toMatchInlineSnapshot(`404`)
        })
      })
    })
  })
})
