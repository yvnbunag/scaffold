import { defaultComposition } from '~/server'
import { server, router } from '@/fastify'
import { root } from '~/routes'
import { createDependencies } from '$/specs/integration/__lib__'

import type { Dependencies } from '#/server'

let app: ReturnType<typeof server.create>

beforeAll(async () => {
  app = server.create<Dependencies>(
    [
      router.create([root]),
      ...defaultComposition,
    ],
    await createDependencies(),
  )
})

describe('Root API', () => {
  describe('GET /version', () => {
    it('should return version', async () => {
      const response = await app.inject({
        method: 'GET',
        path: '/version',
      })

      expect(response.json()).toMatchSnapshot()
      expect(response.statusCode).toMatchInlineSnapshot(`200`)
    })
  })
})
