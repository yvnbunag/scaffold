import * as modelsModule from '~/models'
import { connection } from '@/sequelize'

import type { Dependencies } from '#/server'

export async function createDependencies(): Promise<Dependencies> {
  const config = {
    port: 3010,
    version: '0.0.1',
  }
  const models = modelsModule.create({
    test: connection.initialize(connection.Engine.IN_MEMORY),
  })

  await modelsModule.sync(models)

  return {
    config,
    models,
  }
}
