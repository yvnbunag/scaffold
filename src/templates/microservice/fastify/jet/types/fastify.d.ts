import { Dependencies } from '#/server'

import type { sendStatusJSON } from '@/fastify/send-status-json'
import type { DeepReadonly } from 'ts-essentials'

declare module 'fastify' {
  interface FastifyInstance {
    dependencies: DeepReadonly<Dependencies>,
  }

  interface FastifyReply {
    sendStatusJSON: typeof sendStatusJSON,
  }
}
