import fastifyBasicAuth from 'fastify-basic-auth'
import { UnauthorizedError } from '@/fastify/errors'

import type { FastifyBasicAuthOptions } from 'fastify-basic-auth'
import type { Composite } from '@/fastify/types'

export function create(username: string, password: string): Composite {
  return function registerBasicAuth (app) {
    return app.register(
      fastifyBasicAuth,
      { validate: createValidator(username, password) },
    )
  }
}

export function createValidator(
  configUsername: string,
  configPassword: string,
): FastifyBasicAuthOptions['validate'] {
  return (username, password, request, reply, done) => {
    if (
      username === configUsername &&
      password === configPassword
    ) return done()

    done(new UnauthorizedError())
  }
}
