import type {
  FastifyInstance,
  FastifyPluginCallback,
  RegisterOptions,
  RouteHandlerMethod,
} from 'fastify'
import type { FromSchema, JSONSchema } from 'json-schema-to-ts'

export type Composite = (app: FastifyInstance)=> FastifyInstance

export type Composites = Array<Composite>

export namespace Injection {
  export type Definition<
    Dependencies,
    Keys extends keyof Dependencies & string = keyof Dependencies & string,
  > = { [Key in Keys]: Dependencies[Key] }

  export type Keys<Dependencies> = keyof Dependencies & string
}

export namespace Route {
  export type Register = FastifyPluginCallback

  export type Options = RegisterOptions

  export interface Module {
    register: Register,
    options?: Options,
  }
}

export namespace Schema {
  type JSONToValidationMap = {
    body: 'Body',
    query: 'Querystring',
    params: 'Params',
    headers: 'Headers',
  }

  export type Validation = Partial<
    Record<keyof JSONToValidationMap, JSONSchema>
  >

  export type Domain<Type extends Validation> = {
    [
      Key in keyof Type as Key extends keyof JSONToValidationMap
        ? JSONToValidationMap[Key]
        : never
    ]: FromSchema<Type[Key]>
  }
}

export type Controller<
  DefinedSchema extends Schema.Validation = Record<string, unknown>,
> = RouteHandlerMethod extends RouteHandlerMethod<
  infer RawServer,
  infer RawRequest,
  infer RawReply,
  infer RouteGeneric
> ? RouteHandlerMethod<
    RawServer,
    RawRequest,
    RawReply,
    RouteGeneric & Schema.Domain<DefinedSchema>
  >
  : never

export namespace ErrorHandler {
  export interface Response {
    statusCode?: number,
    error?: string,
    message?: string,
  }

  export type Handle = (response?: Response)=> void

  export type Handler = (error: Error, handle: Handle)=> void
}
