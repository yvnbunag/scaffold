import type { Schema } from '@/fastify/types'

/**
 * Helper to define JSON schema with completion through intellisense. Pass const
 *  asserted definition when output will be used as a type parameter to preserve
 *  footprint and prevent "type instantiation excessively deep" error
 *
 * @example
 * export const schema = define({
 *  body: {
 *    type: 'object',
 *    properties: { key: { type: 'string' } }
 *  },
 * } as const)
 *
 * type MappedSchema = MapSchema<typeof schema>
 */
export function define<
  Definition extends Schema.Validation,
>(schema: Definition): Definition {
  return schema
}
