import fastify from 'fastify'
import { isObject } from 'lodash'
import { ServerError } from '@/fastify/errors'

import type { FastifyInstance } from 'fastify'
import type { Composites, Injection } from '@/fastify/types'

export function create<
  Dependencies extends Injection.Definition<Dependencies>,
>(
  composition: Composites = [],
  dependencies: Dependencies = {} as Dependencies,
): FastifyInstance {
  const app = fastify()

  app.decorate(
    'dependencies',
    createDependencyInjection(dependencies) as unknown,
  )

  for (const compose of composition) compose(app)

  return app
}

export function serve(
  app: FastifyInstance,
  port: number,
  dependencies: {
    process: NodeJS.Process,
    console: Console,
  } = {
    process,
    console,
  },
): SideEffect<FastifyInstance> {
  app.listen(port, (err, address) => {
    if (err) {
      dependencies.console.error(err)
      dependencies.process.exit(1)
    }

    dependencies.console.log(`Server listening at ${address}`)
  })

  return app
}

const injected = Symbol('dependency-injected')

export function createDependencyInjection<
  Dependencies extends Injection.Definition<Dependencies>,
>(dependencies: Dependencies = {} as Dependencies): Dependencies {
  if (!isObject(dependencies)) return dependencies
  if (injected in dependencies) return dependencies

  const dependencyInjection = Object
    .assign({}, dependencies, { [injected]: true })

  return new Proxy(
    dependencyInjection,
    {
      get(
        target,
        dependency: Injection.Keys<Dependencies>,
      ): Dependencies[Injection.Keys<Dependencies>] | undefined | never {
        // Bypass fastify decorate checker if dependency has getters and setters
        if (['getter', 'setter'].includes(dependency)) return target[dependency]

        if (dependency in target) return target[dependency]

        throw new ServerError(`Dependency '${dependency}' is not injected`)
      },
      set(target, dependency: Injection.Keys<Dependencies>) {
        throw new ServerError(
          `Mutation of dependency '${dependency}' forbidden`,
        )
      },
    },
  )
}
