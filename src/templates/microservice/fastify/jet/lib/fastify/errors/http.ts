export class HTTPError extends Error {
  constructor(message?: string, public readonly statusCode = 500) {
    super(message)

    this.name = 'HTTPError'
  }
}
