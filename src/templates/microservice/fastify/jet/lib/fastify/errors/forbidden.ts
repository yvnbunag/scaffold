import { HTTPError } from '@/fastify/errors/http'

export class ForbiddenError extends HTTPError {
  constructor(message = 'forbidden') {
    super(message, 403)

    this.name = 'ForbiddenError'
  }
}
