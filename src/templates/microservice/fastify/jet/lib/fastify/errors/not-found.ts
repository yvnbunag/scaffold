import { HTTPError } from '@/fastify/errors/http'

export class NotFoundError extends HTTPError {
  constructor(message = 'not found') {
    super(message, 404)

    this.name = 'NotFoundError'
  }
}
