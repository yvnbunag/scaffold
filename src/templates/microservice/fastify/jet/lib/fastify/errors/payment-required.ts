import { HTTPError } from '@/fastify/errors/http'

export class PaymentRequiredError extends HTTPError {
  constructor(message = 'payment-required') {
    super(message, 402)

    this.name = 'PaymentRequiredError'
  }
}
