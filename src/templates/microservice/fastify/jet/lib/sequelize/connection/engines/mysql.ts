import { Sequelize } from 'sequelize'

import type { Options as SequelizeOptions } from 'sequelize'
import type { Connection } from '@/sequelize/types'

type Options = Connection.DefaultOptions
  & Required<Pick<SequelizeOptions, 'database' | 'username' | 'password'>>
  & Partial<Pick<SequelizeOptions, 'host'>>

export const create: Connection.CreateInitialize<Options> = (options) => {
  return function initializeMySQLConnection() {
    return new Sequelize({ dialect: 'mysql', ...options })
  }
}
