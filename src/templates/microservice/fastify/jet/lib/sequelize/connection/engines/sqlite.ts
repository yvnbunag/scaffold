import { Sequelize } from 'sequelize'

import type { Options as SequelizeOptions } from 'sequelize'
import type { Connection } from '@/sequelize/types'

type Options = Connection.DefaultOptions
  & Required<Pick<SequelizeOptions, 'storage'>>

export const create: Connection.CreateInitialize<Options> = (options) => {
  return function initializeSQLiteConnection() {
    return new Sequelize({ dialect: 'sqlite', ...options })
  }
}
