import type { Sequelize, Options } from 'sequelize'

export namespace Connection {
  export type Initialize = ()=> Sequelize

  export type DefaultOptions = Pick<Options, 'logging'>

  export type CreateInitialize<
    Options extends DefaultOptions,
  > = Partial<Options> extends Options
    ? (options?: Options)=> Initialize
    : (options: Options)=> Initialize

  export type Factory<Key extends string> = Record<Key, Initialize>
}

export namespace Model {
  export type Initialize = (connection: Sequelize)=> unknown

  export type Factory<
    Key extends string,
    ConnectionKey extends string,
  > = Record<Key, { initialize: Initialize, connection: ConnectionKey}>

  export namespace Attributes {
    export interface Managed {
      createdAt: Date,
      updatedAt: Date,
      deletedAt: Nullable<Date>,
    }
  }
}
