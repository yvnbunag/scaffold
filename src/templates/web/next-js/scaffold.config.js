/**
 * @type {import('@/template').Definition}
 */
const config = {
  display: 'Next.js',
  isDomain: true,
}

module.exports = config
