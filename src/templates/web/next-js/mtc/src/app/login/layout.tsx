import { EmptyServerLayout } from '@/lib/components/EmptyServerLayout';
import { Metadata } from 'next';

export const metadata: Metadata = {
  title: 'Login',
};

export default EmptyServerLayout;
