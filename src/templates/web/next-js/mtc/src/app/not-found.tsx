'use client';

import { Fillable } from '@/lib/components/Fillable';
import { useDictionary } from '@/lib/dictionary';
import { Button, Typography } from '@mui/material';
import Link from 'next/link';

export default function NotFound() {
  const dict = useDictionary();

  return (
    <Fillable alignItems='center' justifyContent='center' fill>
      <Typography variant='h1'>{dict.notFound.heading}</Typography>
      <Typography variant='body1' marginBottom={4}>
        {dict.notFound.body}
      </Typography>
      <Link data-testid='notFound-button-goto-home' href='/'>
        <Button variant='contained'>{dict.notFound.button.goToHome}</Button>
      </Link>
    </Fillable>
  );
}
