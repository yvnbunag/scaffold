'use client';

import { Fillable } from '@/lib/components/Fillable';
import { useDictionary } from '@/lib/dictionary';
import { Typography } from '@mui/material';

export default function Page() {
  const dict = useDictionary();

  return (
    <Fillable fill alignItems='center' justifyContent='center'>
      <Typography variant='h3'>{dict.visualization.header}</Typography>
    </Fillable>
  );
}
