'use client';

import { FillLoader } from '@/lib/components/FillLoader';
import { useDictionary } from '@/lib/dictionary';
import { useTheme } from '@mui/material';
import { useMemo } from 'react';
import { Chart } from 'react-google-charts';

type SalesProps = {
  sales: Array<[number, number | null]>;
  loading: boolean;
};

export function Sales({ sales, loading }: SalesProps) {
  const dict = useDictionary();
  const theme = useTheme();
  const data = useMemo(() => {
    return [
      [dict.landing.sales.time, dict.landing.sales.sales],
      ...sales.map(([time, sale]) => [new Date(time), sale]),
    ];
  }, [dict, sales]);

  if (loading) {
    return <FillLoader />;
  }

  return (
    <Chart
      chartType='Line'
      data={data}
      options={{
        series: [{ color: theme.palette.secondary.main }],
      }}
    />
  );
}
