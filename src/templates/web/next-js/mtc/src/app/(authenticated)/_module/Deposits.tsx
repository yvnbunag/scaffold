'use client';

import { FillLoader } from '@/lib/components/FillLoader';
import { substitute, useDictionary } from '@/lib/dictionary';
import { formatCurrency, formatDate } from '@/lib/format';
import { Box, Typography } from '@mui/material';

type DepositsProps = {
  deposits: number;
  date: number;
  loading: boolean;
};

export function Deposits({ deposits, date, loading }: DepositsProps) {
  const dict = useDictionary();

  if (loading) {
    return <FillLoader />;
  }

  return (
    <Box>
      <Typography component='p' variant='h4'>
        {formatCurrency(deposits)}
      </Typography>
      <Typography>
        {substitute(dict.landing.deposits.depositsOn, {
          date: formatDate(new Date(date)),
        })}
      </Typography>
    </Box>
  );
}
