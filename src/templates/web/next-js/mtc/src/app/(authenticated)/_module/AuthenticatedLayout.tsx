'use client';

import { Fillable } from '@/lib/components/Fillable';
import { Logo } from '@/lib/components/Logo';
import { NavigationLink } from '@/lib/components/NavigationLink';
import { useDictionary } from '@/lib/dictionary';
import { useMenu } from '@/lib/hooks/useMenu';
import { LogoutIcon } from '@/lib/icons';
import {
  AppBar,
  Avatar,
  Box,
  IconButton,
  ListItemIcon,
  Menu,
  MenuItem,
  Toolbar,
} from '@mui/material';
import { useRouter } from 'next/navigation';

export function AuthenticatedLayout({ children }: React.PropsWithChildren) {
  const dict = useDictionary();
  const menu = useMenu();
  const router = useRouter();
  return (
    <Fillable fill>
      <AppBar position='static'>
        <Toolbar
          variant='regular'
          sx={{
            display: 'flex',
            justifyContent: 'space-between',
          }}>
          <Box display='flex' alignItems='center'>
            <Logo data-testid='appbar-button-logo' href='/' />
            <NavigationLink
              data-testid='appbar-button-goto-visualization'
              href='/visualization'>
              {dict.navigation.button.goToVisualization}
            </NavigationLink>
            <NavigationLink
              data-testid='appbar-button-goto-reports'
              href='/reports'>
              {dict.navigation.button.goToReports}
            </NavigationLink>
          </Box>
          <IconButton data-testid='appbar-button-avatar' onClick={menu.open}>
            <Avatar sx={{ width: 36, height: 36 }} />
          </IconButton>
        </Toolbar>
      </AppBar>
      <Menu
        anchorEl={menu.anchorEl}
        open={menu.isOpen}
        onClose={menu.close}
        onClick={menu.close}>
        <MenuItem
          data-testid='appbar-avatar-menu-button-logout'
          onClick={() => router.push('/logout')}>
          <ListItemIcon>
            <LogoutIcon fontSize='small' />
          </ListItemIcon>
          {dict.navigation.avatarMenu.button.logout}
        </MenuItem>
      </Menu>
      {children}
    </Fillable>
  );
}
