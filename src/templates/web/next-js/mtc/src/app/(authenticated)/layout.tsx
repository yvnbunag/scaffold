import { AuthenticatedLayout } from '@/app/(authenticated)/_module/AuthenticatedLayout';

export default function Layout({ children }: React.PropsWithChildren) {
  return <AuthenticatedLayout>{children}</AuthenticatedLayout>;
}
