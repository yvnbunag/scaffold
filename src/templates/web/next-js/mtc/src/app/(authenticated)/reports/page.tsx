'use client';

import { useReports } from '@/clients/api';
import { FillLoader } from '@/lib/components/FillLoader';
import { Fillable } from '@/lib/components/Fillable';
import { useDictionary } from '@/lib/dictionary';
import { Typography } from '@mui/material';
import { useState } from 'react';

export default function Page() {
  const dict = useDictionary();
  const [today] = useState(new Date());
  const reports = useReports({ date: today });

  if (reports.loading) {
    return <FillLoader />;
  }

  return (
    <Fillable fill alignItems='center' justifyContent='center'>
      <Typography variant='h3'>{dict.reports.header}</Typography>
    </Fillable>
  );
}
