import { EmptyServerLayout } from '@/lib/components/EmptyServerLayout';
import { Metadata } from 'next/types';

export const metadata: Metadata = {
  title: 'Logout',
};

export default EmptyServerLayout;
