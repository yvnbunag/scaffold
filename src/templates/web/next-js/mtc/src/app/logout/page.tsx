'use client';

import { useLogout } from '@/clients/auth';
import { FillLoader } from '@/lib/components/FillLoader';
import { useRouter } from 'next/navigation';
import { useEffect } from 'react';

export default function Page() {
  const router = useRouter();
  const logout = useLogout(() => router.push('/login'));

  useEffect(() => {
    logout.mutate();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  return <FillLoader />;
}
