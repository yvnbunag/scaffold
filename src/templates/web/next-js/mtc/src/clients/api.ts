import { ApiError, defaultQueryOptions } from '@/clients/config';
import { useInfiniteQuery, useQuery } from '@tanstack/react-query';
import { useEffect, useState } from 'react';

const apiUrl = process.env.NEXT_PUBLIC_API_URL || '';

type SalesQuery = {
  date: Date;
};
type SalesData = {
  sales: Array<[number, number | null]>;
  date: number;
};
export function useSales({ date }: SalesQuery) {
  const query = useQuery<SalesData>({
    ...defaultQueryOptions,
    initialData: { sales: [], date: 0 },
    queryKey: ['sales', date],
    queryFn: () =>
      parseResponse(fetch(`${apiUrl}/sales?date=${date.getTime()}`)),
  });

  return { data: query.data, loading: query.isFetching };
}

type DepositsQuery = {
  date: Date;
};
type DepositsData = {
  deposits: number;
  date: number;
};
export function useDeposits({ date }: DepositsQuery) {
  const query = useQuery<DepositsData>({
    ...defaultQueryOptions,
    initialData: { deposits: 0, date: 0 },
    queryKey: ['deposits', date],
    queryFn: () =>
      parseResponse(fetch(`${apiUrl}/deposits?date=${date.getTime()}`)),
  });

  return { data: query.data, loading: query.isFetching };
}

type OrdersQuery = {
  date: Date;
};
type OrderData = {
  id: number;
  date: number;
  name: string;
  shipTo: string;
  paymentMethod: {
    provider: string;
    lastFourDigits: string;
  };
  amount: number;
};
type OrdersData = {
  orders: OrderData[];
  date: number;
  hasNextPage: boolean;
  nextCursor: number | null;
};
export function useOrders({ date }: OrdersQuery) {
  const [page, setPage] = useState(0);
  const query = useInfiniteQuery<OrdersData>({
    ...defaultQueryOptions,
    initialData: { pages: [], pageParams: [] },
    queryKey: ['orders', date],
    getNextPageParam: (lastPage) => {
      if (!lastPage) {
        return null;
      }

      if (lastPage.hasNextPage) {
        return lastPage.nextCursor;
      }

      return undefined;
    },
    queryFn: ({ pageParam }) => {
      const url = new URL(`${apiUrl}/orders?date=${date.getTime()}`);

      if (pageParam) {
        url.searchParams.set('cursor', pageParam);
      }

      return parseResponse(fetch(url));
    },
  });
  const currentPages = query.data?.pages.length || 1;
  const pages = query.hasNextPage ? currentPages + 1 : currentPages;
  const data = {
    orders: query.data?.pages[page]?.orders || [],
    date: query.data?.pages[page]?.date || 0,
  };

  useEffect(() => {
    if (page >= currentPages) {
      query.fetchNextPage();
    }
  }, [page, currentPages, query]);

  return {
    pages,
    page: page + 1,
    setPage: (page: number) => setPage(page - 1),
    data,
    loading: query.isFetching || query.isFetchingNextPage,
  };
}

type ReportsQuery = {
  date: Date;
};
export function useReports({ date }: ReportsQuery) {
  const query = useQuery({
    ...defaultQueryOptions,
    queryKey: ['reports', date],
    queryFn: () =>
      parseResponse(fetch(`${apiUrl}/reports?date=${date.getTime()}`)),
  });

  return { data: query.data, loading: query.isFetching };
}

async function parseResponse(responsePromise: Promise<Response>) {
  const response = await responsePromise;

  if (response.status < 400) {
    const { data } = await response.json();

    return data;
  }

  throw await ApiError.from(response);
}
