'use client';

import { getDictionary, substitute } from '@/lib/dictionary';
import { Dispatch, SetStateAction, useCallback, useState } from 'react';

type ValidationError = string | undefined;
type Validation<Value> = (name: string, value: Value) => ValidationError;
type Field<Value> = {
  value: Value;
  setValue: Dispatch<SetStateAction<Value>>;
  validate: () => boolean;
  hasError: boolean;
  errorMessage: ValidationError;
};
export function useField<Value>(
  name: string,
  initialValue: Value,
  validations: Validation<Value>[] = [],
): Field<Value> {
  const [value, setValue] = useState(initialValue);
  const [errorMessage, setErrorMessage] = useState<ValidationError>();
  const validate = useCallback(
    (name: string, value: Value) => {
      for (const validation of validations) {
        const validationError = validation(name, value);

        if (validationError) {
          setErrorMessage(validationError);
          return false;
        }
      }

      setErrorMessage(undefined);
      return true;
    },
    [validations],
  );

  return {
    value,
    setValue,
    validate: () => validate(name, value),
    hasError: Boolean(errorMessage),
    errorMessage: errorMessage,
  };
}

export const EMPTY_STRING: string = '';

export const notEmpty: Validation<string> = (field, value) => {
  const dict = getDictionary();

  if (value === EMPTY_STRING) {
    return substitute(dict.validation.string.notEmpty, { field });
  }
};

const emailPattern =
  /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
export const validEmail: Validation<string> = (field, value) => {
  const dict = getDictionary();

  if (!value.match(emailPattern)) {
    return substitute(dict.validation.string.validEmail, { field });
  }
};
