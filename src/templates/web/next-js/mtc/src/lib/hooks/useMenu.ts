import { useState } from 'react';

export function useMenu() {
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  function open(event: React.MouseEvent<HTMLElement>) {
    setAnchorEl(event.currentTarget);
  }
  function close() {
    setAnchorEl(null);
  }

  return {
    anchorEl,
    isOpen: Boolean(anchorEl),
    open,
    close,
  };
}
