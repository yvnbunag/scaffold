import AnalyticsIcon from '@mui/icons-material/Analytics';
import CloseIcon from '@mui/icons-material/Close';
import CloudOffIcon from '@mui/icons-material/CloudOff';
import LockPersonIcon from '@mui/icons-material/LockPerson';
import LoginIcon from '@mui/icons-material/Login';
import LogoutIcon from '@mui/icons-material/Logout';

export {
  AnalyticsIcon,
  LogoutIcon,
  CloseIcon,
  LockPersonIcon,
  LoginIcon,
  CloudOffIcon,
};
