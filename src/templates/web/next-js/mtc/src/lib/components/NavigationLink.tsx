'use client';

import { Button, Typography } from '@mui/material';
import Link from 'next/link';
import { usePathname } from 'next/navigation';
import { ComponentProps } from 'react';

export function NavigationLink({
  children,
  ...linkProps
}: React.PropsWithChildren<ComponentProps<typeof Link>>) {
  const pathname = usePathname();
  const inPath = pathname === linkProps.href;

  return (
    <Link {...linkProps} style={{ cursor: 'default' }}>
      <Button variant='text' sx={{ cursor: inPath ? 'default' : undefined }}>
        <Typography
          textAlign='center'
          sx={{
            textDecoration: inPath ? 'underline' : undefined,
            textUnderlineOffset: 4,
          }}
          color='primary.contrastText'>
          {children}
        </Typography>
      </Button>
    </Link>
  );
}
