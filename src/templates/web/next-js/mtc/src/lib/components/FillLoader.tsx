'use client';

import { Fillable } from '@/lib/components/Fillable';
import CircularProgress from '@mui/material/CircularProgress';

export function FillLoader() {
  return (
    <Fillable justifyContent='center' alignItems='center' fill>
      <CircularProgress />
    </Fillable>
  );
}
