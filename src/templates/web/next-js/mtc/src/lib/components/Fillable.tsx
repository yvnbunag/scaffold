'use client';

import { Box, BoxProps } from '@mui/material';

type FillableProps = {
  fill?: boolean;
};

export function Fillable({
  children,
  fill,
  ...boxProps
}: FillableProps & BoxProps) {
  return (
    <Box
      display='flex'
      flexDirection='column'
      flexGrow={fill ? 1 : undefined}
      {...boxProps}>
      {children}
    </Box>
  );
}

export function FillableBody({
  children,
  ...bodyProps
}: React.PropsWithChildren<JSX.IntrinsicElements['body']>) {
  return (
    <body
      style={{
        height: '100vh',
        display: 'flex',
        flexDirection: 'column',
      }}
      {...bodyProps}>
      {children}
    </body>
  );
}
