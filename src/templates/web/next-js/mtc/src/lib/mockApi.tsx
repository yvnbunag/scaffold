'use client';

import { CloudOffIcon } from '@/lib/icons';
import { Fab, Tooltip } from '@mui/material';
import { useCallback, useEffect, useState } from 'react';

export function MockApi() {
  const [mockApiIsReady, setMockApiIsReady] = useState(false);
  const pollMockApi = useCallback(async (onReady: () => void) => {
    try {
      const response = await fetch('https://mock-api/ready', {
        cache: 'no-store',
      });

      if (response.status < 400) {
        onReady();
      }
    } catch {}
  }, []);

  useEffect(() => {
    let pollMockApiInterval: NodeJS.Timer;
    if (mockApiIsReady) {
      return;
    }
    const onReady = () => {
      setMockApiIsReady(true);
      clearInterval(pollMockApiInterval);
    };

    pollMockApi(onReady);
    pollMockApiInterval = setInterval(() => pollMockApi(onReady), 500);

    () => clearInterval(pollMockApiInterval);
  }, [mockApiIsReady, pollMockApi]);

  useEffect(() => {
    (async () => {
      // https://developer.chrome.com/docs/workbox/handling-service-worker-updates/
      if ('serviceWorker' in navigator) {
        const { Workbox } = await import('workbox-window');
        const workbox = new Workbox('mock-api.js');

        // https://stackoverflow.com/a/62596701
        workbox.register().then((registration) => {
          // There's an active SW, but no controller for this tab.
          if (registration?.active && !navigator.serviceWorker.controller) {
            window.location.reload();
          }
        });
      }
    })();
  }, []);

  return mockApiIsReady ? null : <MockApiIndicatorNotReady />;
}

function MockApiIndicatorNotReady() {
  return (
    <Tooltip title='Mock API not ready'>
      <Fab
        data-testid='mock-api-indicator-not-ready'
        sx={{
          position: 'fixed',
          bottom: 16,
          right: 16,
          '&:hover': { cursor: 'default' },
        }}
        size='small'
        disableFocusRipple
        disableRipple>
        <CloudOffIcon />
      </Fab>
    </Tooltip>
  );
}
