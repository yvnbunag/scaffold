importScripts(
  'https://storage.googleapis.com/workbox-cdn/releases/7.0.0/workbox-sw.js',
);

const hostname = 'mock-api';
const responses = {
  ready: {
    get: createJsonResponse,
  },
  login: {
    post: {
      'incorrect-password': () =>
        createJsonResponse(401, {
          success: false,
          error: 'unauthorized',
        }),
      'internal-error': () =>
        createJsonResponse(500, {
          success: false,
          error: "Cannot read properties of null (reading 'parse')",
        }),
      default: () =>
        createJsonResponse(200, {
          success: true,
          data: { token: '6e52e97b-9557-49da-9c6b-8d3427517126' },
        }),
    },
  },
  logout: { post: createJsonResponse },
  sales: {
    get: (date) =>
      createJsonResponse(200, {
        success: true,
        data: {
          sales: [
            [createTimestampNow('00:00'), 0],
            [createTimestampNow('03:00'), 300],
            [createTimestampNow('06:00'), 600],
            [createTimestampNow('09:00'), 800],
            [createTimestampNow('12:00'), 1500],
            [createTimestampNow('15:00'), 2000],
            [createTimestampNow('18:00'), 2400],
            [createTimestampNow('21:00'), 2400],
            [createTimestampNow('24:00'), null],
          ],
          date: date.getTime(),
        },
      }),
  },
  deposits: {
    get: (date) =>
      createJsonResponse(200, {
        success: true,
        data: { deposits: 3024, date: date.getTime() },
      }),
  },
  orders: {
    get: {
      0: () =>
        createJsonResponse(200, {
          success: true,
          data: {
            orders: [
              {
                id: 1,
                date: createTimestampNow('01:00'),
                name: 'Johnny Cash',
                shipTo: 'Kingsland, AR',
                paymentMethod: {
                  provider: 'MC',
                  lastFourDigits: '5120',
                },
                amount: 399.99,
              },
              {
                id: 2,
                date: createTimestampNow('01:30'),
                name: 'Nina Simone',
                shipTo: 'Tryon, NC',
                paymentMethod: {
                  provider: 'VISA',
                  lastFourDigits: '8421',
                },
                amount: 249.0,
              },
              {
                id: 3,
                date: createTimestampNow('02:00'),
                name: 'Jim Morrison',
                shipTo: 'Melbourne, FL',
                paymentMethod: {
                  provider: 'MC',
                  lastFourDigits: '6342',
                },
                amount: 99.99,
              },
              {
                id: 4,
                date: createTimestampNow('02:30'),
                name: 'Amy Winehouse',
                shipTo: 'Southgate, London',
                paymentMethod: {
                  provider: 'VISA',
                  lastFourDigits: '4689',
                },
                amount: 199.5,
              },
              {
                id: 5,
                date: createTimestampNow('03:00'),
                name: 'Lionel Richie',
                shipTo: 'Tuskegee, AL',
                paymentMethod: {
                  provider: 'MC',
                  lastFourDigits: '2875',
                },
                amount: 149.0,
              },
              {
                id: 6,
                date: createTimestampNow('03:30'),
                name: 'Mariah Carey',
                shipTo: 'Huntington, NY',
                paymentMethod: {
                  provider: 'VISA',
                  lastFourDigits: '6374',
                },
                amount: 299.99,
              },
              {
                id: 7,
                date: createTimestampNow('04:00'),
                name: 'Eminem',
                shipTo: 'Detroit, MI',
                paymentMethod: {
                  provider: 'MC',
                  lastFourDigits: '5421',
                },
                amount: 199.0,
              },
              {
                id: 8,
                date: createTimestampNow('04:30'),
                name: 'Rihanna',
                shipTo: 'St. Michael, Barbados',
                paymentMethod: {
                  provider: 'VISA',
                  lastFourDigits: '1195',
                },
                amount: 399.5,
              },
              {
                id: 9,
                date: createTimestampNow('05:00'),
                name: 'Freddy Mercury',
                shipTo: 'Stone Town, Zanzibar',
                paymentMethod: {
                  provider: 'MC',
                  lastFourDigits: '8711',
                },
                amount: 799.99,
              },
              {
                id: 10,
                date: createTimestampNow('05:30'),
                name: 'Elton John',
                shipTo: 'Pinner, UK',
                paymentMethod: {
                  provider: 'VISA',
                  lastFourDigits: '2748',
                },
                amount: 299.0,
              },
            ],
            hasNextPage: true,
            nextCursor: '1',
          },
        }),
      1: () =>
        createJsonResponse(200, {
          success: true,
          data: {
            orders: [
              {
                id: 11,
                date: createTimestampNow('06:00'),
                name: 'Adele',
                shipTo: 'Tottenham, London',
                paymentMethod: {
                  provider: 'MC',
                  lastFourDigits: '7329',
                },
                amount: 599.99,
              },
              {
                id: 12,
                date: createTimestampNow('06:30'),
                name: 'Bob Dylan',
                shipTo: 'Duluth, MN',
                paymentMethod: {
                  provider: 'VISA',
                  lastFourDigits: '5236',
                },
                amount: 199.0,
              },
              {
                id: 13,
                date: createTimestampNow('07:00'),
                name: 'Frank Sinatra',
                shipTo: 'Hoboken, NJ',
                paymentMethod: {
                  provider: 'MC',
                  lastFourDigits: '8456',
                },
                amount: 149.5,
              },
              {
                id: 14,
                date: createTimestampNow('07:30'),
                name: 'Beyoncé',
                shipTo: 'Houston, TX',
                paymentMethod: {
                  provider: 'VISA',
                  lastFourDigits: '3092',
                },
                amount: 799.99,
              },
              {
                id: 15,
                date: createTimestampNow('08:00'),
                name: 'Kurt Cobain',
                shipTo: 'Aberdeen, WA',
                paymentMethod: {
                  provider: 'MC',
                  lastFourDigits: '1239',
                },
                amount: 179.0,
              },
              {
                id: 16,
                date: createTimestampNow('08:30'),
                name: 'Whitney Houston',
                shipTo: 'Newark, NJ',
                paymentMethod: {
                  provider: 'VISA',
                  lastFourDigits: '9408',
                },
                amount: 249.99,
              },
              {
                id: 17,
                date: createTimestampNow('09:00'),
                name: 'Bruce Springsteen',
                shipTo: 'Freehold, NJ',
                paymentMethod: {
                  provider: 'MC',
                  lastFourDigits: '6082',
                },
                amount: 129.0,
              },
              {
                id: 18,
                date: createTimestampNow('09:30'),
                name: 'Madonna',
                shipTo: 'Bay City, MI',
                paymentMethod: {
                  provider: 'VISA',
                  lastFourDigits: '7801',
                },
                amount: 299.5,
              },
              {
                id: 19,
                date: createTimestampNow('10:00'),
                name: 'Prince',
                shipTo: 'Minneapolis, MN',
                paymentMethod: {
                  provider: 'MC',
                  lastFourDigits: '2975',
                },
                amount: 199.99,
              },
              {
                id: 20,
                date: createTimestampNow('10:30'),
                name: 'Tina Turner',
                shipTo: 'Nutting Hill, TN',
                paymentMethod: {
                  provider: 'VISA',
                  lastFourDigits: '5337',
                },
                amount: 699.99,
              },
            ],
            hasNextPage: true,
            nextCursor: '2',
          },
        }),
      2: () =>
        createJsonResponse(200, {
          success: true,
          data: {
            orders: [
              {
                id: 21,
                date: createTimestampNow('11:00'),
                name: 'Michael Jackson',
                shipTo: 'Gary, IN',
                paymentMethod: {
                  provider: 'MC',
                  lastFourDigits: '4096',
                },
                amount: 499.0,
              },
              {
                id: 22,
                date: createTimestampNow('11:30'),
                name: 'Janis Joplin',
                shipTo: 'Port Arthur, TX',
                paymentMethod: {
                  provider: 'VISA',
                  lastFourDigits: '1904',
                },
                amount: 599.99,
              },
              {
                id: 23,
                date: createTimestampNow('12:00'),
                name: 'Eric Clapton',
                shipTo: 'London, UK',
                paymentMethod: {
                  provider: 'MC',
                  lastFourDigits: '2367',
                },
                amount: 799.0,
              },
              {
                id: 24,
                date: createTimestampNow('12:30'),
                name: 'Stevie Nicks',
                shipTo: 'Los Angeles, CA',
                paymentMethod: {
                  provider: 'VISA',
                  lastFourDigits: '6578',
                },
                amount: 199.5,
              },
              {
                id: 25,
                date: createTimestampNow('13:00'),
                name: 'Aretha Franklin',
                shipTo: 'Detroit, MI',
                paymentMethod: {
                  provider: 'MC',
                  lastFourDigits: '8123',
                },
                amount: 299.99,
              },
              {
                id: 26,
                date: createTimestampNow('13:30'),
                name: 'David Bowie',
                shipTo: 'London, UK',
                paymentMethod: {
                  provider: 'VISA',
                  lastFourDigits: '4210',
                },
                amount: 150.0,
              },
              {
                id: 27,
                date: createTimestampNow('14:00'),
                name: 'Jimi Hendrix',
                shipTo: 'Seattle, WA',
                paymentMethod: {
                  provider: 'MC',
                  lastFourDigits: '7054',
                },
                amount: 249.75,
              },
              {
                id: 28,
                date: createTimestampNow('14:30'),
                name: 'John Lennon',
                shipTo: 'Liverpool, UK',
                paymentMethod: {
                  provider: 'VISA',
                  lastFourDigits: '9831',
                },
                amount: 599.99,
              },
              {
                id: 29,
                date: createTimestampNow('15:00'),
                name: 'Tom Scholz',
                shipTo: 'Boston, MA',
                paymentMethod: {
                  provider: 'MC',
                  lastFourDigits: '1253',
                },
                amount: 100.81,
              },
              {
                id: 30,
                date: createTimestampNow('15:30'),
                name: 'Paul McCartney',
                shipTo: 'London, UK',
                paymentMethod: {
                  provider: 'VISA',
                  lastFourDigits: '2574',
                },
                amount: 866.99,
              },
            ],
            hasNextPage: false,
            nextCursor: null,
          },
        }),
      default: () =>
        createJsonResponse(200, {
          success: true,
          data: {
            orders: [],
            hasNextPage: false,
            nextCursor: null,
          },
        }),
    },
  },
  reports: {
    get: () =>
      createJsonResponse(503, { success: false, error: 'unavailable' }),
  },
};

// Configure service worker to take control on first load.
// See https://stackoverflow.com/q/41009167.
self.addEventListener('install', function (event) {
  event.waitUntil(self.skipWaiting());
});
self.addEventListener('activate', function (event) {
  event.waitUntil(self.clients.claim());
});

workbox.routing.registerRoute(match('/ready'), () => responses.ready.get());

workbox.routing.registerRoute(
  match('/login'),
  async ({ request }) => {
    const { password } = await request.json();
    await delay(300);
    return (responses.login.post[password] || responses.login.post.default)();
  },
  'POST',
);

workbox.routing.registerRoute(
  match('/logout'),
  async () => {
    await delay(300);
    return responses.logout.post();
  },
  'POST',
);

workbox.routing.registerRoute(match('/sales'), async ({ url }) => {
  await delay(350);
  return responses.sales.get(new Date(Number(url.searchParams.get('date'))));
});

workbox.routing.registerRoute(match('/deposits'), async ({ url }) => {
  await delay(150);
  return responses.deposits.get(new Date(Number(url.searchParams.get('date'))));
});

workbox.routing.registerRoute(match('/orders'), async ({ url }) => {
  const cursor = url.searchParams.get('cursor') || '0';
  await delay(250);
  return (responses.orders.get[cursor] || responses.orders.get.default)();
});

workbox.routing.registerRoute(match('/reports'), async ({ url }) => {
  await delay(100);
  return responses.reports.get();
});

function match(pathname) {
  return ({ url }) => url.hostname === hostname && url.pathname === pathname;
}

function delay(durationMs) {
  return new Promise((resolve) => setTimeout(resolve, durationMs));
}

function createJsonResponse(
  status = 200,
  responseBody = { success: status < 400 },
) {
  const responseBodyBlob = new Blob([JSON.stringify(responseBody)], {
    type: 'application/json',
  });
  return new Response(responseBodyBlob, { status });
}

function createTimestampNow(time) {
  const now = new Date();
  const [hours, minutes] = time.split(':');

  now.setHours(+hours);
  now.setMinutes(+minutes);
  now.setSeconds(0);
  now.setMilliseconds(0);

  return now.getTime();
}
