# <package-name>

A frontend project built with [Next.js](https://nextjs.org/), and powered by
[Material UI](https://mui.com/), [TypeScript](https://www.typescriptlang.org/)
and [Cypress](https://www.cypress.io/).

<!-- omit in toc -->
## Contents

- [Requirements](#requirements)
- [Scaffold setup](#scaffold-setup)
- [Usage](#usage)
  - [Running tests](#running-tests)
  - [Development](#development)
  - [Production](#production)
  - [Linting code](#linting-code)
  - [Formatting code](#formatting-code)
- [Scaffold](#scaffold)

<br/>

## Requirements

1. [Node.js](https://nodejs.org/en/) version 18.15.0 or higher
2. [pnpm](https://pnpm.io/) version 8.3.1 or higher
   - May be swapped with other package managers such as
     [npm](https://docs.npmjs.com/) and [yarn](https://yarnpkg.com/)

<br/>

## Scaffold setup

1. Initialize git repository

```sh
git init
git add .
```

2. Install dependencies

```sh
pnpm install
```

3. Commit and push project to remote repository

```sh
git commit -m "<commit-message>"
git remote add origin <remote-origin>
git push --set-upstream origin <branch-name>
```

<br/>

## Usage

### Running tests

```sh
# Unit tests
pnpm test:unit

# Integration tests with UI
pnpm test:integration:ui

# Headless integration tests
pnpm test:integration
```

### Development

Start service in watched mode

```sh
pnpm dev
```

#### Mock server

Network requests are simulated by intercepting requests through a service
worker, effectively mimicking the behavior of actual network communication.

To disable it, remove the provider from
[src/app/layout.tsx](src/app/layout.tsx). Additionally, if you have running
service workers, you will need to
[manually remove them from your browser](https://stackoverflow.com/a/33705250).

### Production

Build production package

```sh
pnpm build
```

Start service from production package

```sh
pnpm start
```

### Linting code

```sh
pnpm lint
```

### Formatting code

```sh
pnpm format
```

## Scaffold

Project scaffolded with [@ianbunag/scaffold](https://www.npmjs.com/package/@ianbunag/scaffold)
