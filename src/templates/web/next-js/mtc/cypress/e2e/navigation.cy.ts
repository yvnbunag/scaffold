describe('landing', () => {
  it('should navigate to visualization', () => {
    cy.visit('/');
    cy.getByTestId('appbar-button-goto-visualization').click();
    cy.get('h3').contains('Visualization').should('be.visible');
  });

  it('should show not found page', () => {
    cy.visit('/not-found', { failOnStatusCode: false });
    cy.getByTestId('appbar-button-logo').should('not.exist');
    cy.contains('404').should('be.visible');
    cy.contains('The page you are looking for does not exist!').should(
      'be.visible',
    );

    cy.getByTestId('notFound-button-goto-home').click();
    cy.getByTestId('appbar-button-logo').should('be.visible');
    cy.contains('404').should('not.exist');
  });
});
