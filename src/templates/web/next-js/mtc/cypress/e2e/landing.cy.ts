describe('landing', () => {
  const patterns = {
    currency: /^\$[\d,]+\.\d{2}$/,
  };

  beforeEach(() => {
    cy.visit('/');
  });

  describe('sales', () => {
    it('should be visible', () => {
      cy.getByTestId('landing-card-sales').within(() => {
        cy.contains('Today').should('be.visible');
        cy.contains('Sales ($)').should('be.visible');
      });
    });
  });

  describe('deposits', () => {
    it('should be visible', () => {
      cy.getByTestId('landing-card-deposits').within(() => {
        cy.contains('Recent Deposits').should('be.visible');
        cy.contains(patterns.currency).should('be.visible');
      });
    });
  });

  describe('orders', () => {
    it('should be visible', () => {
      cy.getByTestId('landing-card-orders').within(() => {
        cy.contains('Recent Orders').should('be.visible');
        cy.getPagination(1).should('be.visible');
        cy.contains(patterns.currency).should('be.visible');
      });
    });

    it('should paginate', () => {
      cy.getByTestId('landing-card-orders').within(() => {
        cy.contains('01:00 AM').should('be.visible');
        cy.contains('05:30 AM').should('be.visible');
        cy.getPagination(3).should('not.exist');
        cy.getPagination(2).click();

        cy.contains('06:00 AM').should('be.visible');
        cy.contains('10:30 AM').should('be.visible');
        cy.getPagination(4).should('not.exist');
        cy.getPagination(3).click();

        cy.contains('11:00 AM').should('be.visible');
        cy.contains('03:30 PM').should('be.visible');
        cy.getPagination(4).should('not.exist');
      });
    });
  });
});
