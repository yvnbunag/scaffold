describe('login', () => {
  beforeEach(() => {
    cy.visit('/login');
  });

  it('should log in successfully', () => {
    cy.getByTestId('login-input-username').type('test@example.com');
    cy.getByTestId('login-input-password').type('correct-password');
    cy.getByTestId('login-button-login').click();
    cy.getByTestId('login-button-login').should('be.disabled');
    cy.contains('Today').should('be.visible');
    cy.getByTestId('login-button-login').should('not.exist');
  });

  it('should log in unsucessfully', () => {
    cy.getByTestId('login-input-username').type('test@example.com');
    cy.getByTestId('login-input-password').type('incorrect-password');
    cy.getByTestId('login-button-login').click();
    cy.getByTestId('login-button-login').should('be.disabled');
    cy.contains('Incorrect username or password.').should('be.visible');
    cy.getByTestId('login-button-login').should('exist');
  });

  it('should require valid email', () => {
    cy.getByTestId('login-input-username').type('username');
    cy.getByTestId('login-input-password').type('password');
    cy.getByTestId('login-button-login').click();
    cy.contains('Username must be a valid email address.').should('be.visible');
    cy.getByTestId('login-button-login').should('exist');
  });

  it('should require email', () => {
    cy.getByTestId('login-input-password').type('password');
    cy.getByTestId('login-button-login').click();
    cy.contains('Username must not be empty.').should('be.visible');
    cy.getByTestId('login-button-login').should('exist');
  });

  it('should require password', () => {
    cy.getByTestId('login-input-username').type('test@example.com');
    cy.getByTestId('login-button-login').click();
    cy.contains('Password must not be empty.').should('be.visible');
    cy.getByTestId('login-button-login').should('exist');
  });

  it('should notify about server errors', () => {
    cy.getByTestId('login-input-username').type('test@example.com');
    cy.getByTestId('login-input-password').type('internal-error');
    cy.getByTestId('login-button-login').click();
    cy.getByTestId('login-button-login').should('be.disabled');
    cy.contains('Internal server error.').should('be.visible');
    cy.getByTestId('login-button-login').should('exist');
  });

  it('should log out', () => {
    cy.getByTestId('login-input-username').type('test@example.com');
    cy.getByTestId('login-input-password').type('correct-password');
    cy.getByTestId('login-button-login').click();
    cy.getByTestId('login-button-login').should('not.exist');
    cy.getByTestId('appbar-button-avatar').click();
    cy.getByTestId('appbar-avatar-menu-button-logout').click();
    cy.getByTestId('login-button-login').should('be.visible');
  });
});
