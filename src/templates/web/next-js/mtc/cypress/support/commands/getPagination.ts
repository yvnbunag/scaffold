Cypress.Commands.add('getPagination', (page) => {
  return cy.get(`nav[aria-label="pagination navigation"]`).contains(page);
});

declare global {
  namespace Cypress {
    interface Chainable<Subject> {
      /**
       * @example cy.getPagination(1);
       */
      getPagination(page: number): Chainable<Subject>;
    }
  }
}

export {};
