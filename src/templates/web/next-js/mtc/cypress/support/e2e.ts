import './commands/getByTestId';
import './commands/getPagination';

before(() => {
  // Ensure mock server is activated before all tests.
  cy.visit('/');
});
