const { getUserName, getUserEmail } = require('~/git')

const username = getUserName()
const email = getUserEmail()

const file = 'scaffold.config.js'
const excluded = [file]
const renames = [
  ['.npmignore', '.gitignore'],
  ['temp/.npmignore', 'temp/.gitignore'],
  ['storage/.npmignore', 'storage/.gitignore'],
]

/**
 * @type {import('@/template').Definition["replacements"]}
 */
const standardReplacements = [
  {
    display: 'Package name',
    pattern: '<package-name>',
    value: ''
  },
  {
    display: 'Package version',
    pattern: '<package-version>',
    value: '0.0.1'
  },
  {
    display: 'Package description',
    pattern: '<package-description>',
    value: ''
  },
  {
    display: 'Package author',
    pattern: '<package-author>',
    value: username && email ? `${username} <${email}>`.trim() : ''
  },
  {
    display: 'MIT License Year',
    pattern: '<mit-license-year>',
    value: new Date().getFullYear().toString()
  },
  {
    display: 'MIT License Author',
    pattern: '<mit-license-author>',
    value: username || '<mit-license-author>'
  }
]

module.exports = {
  file,
  excluded,
  renames,
  standardReplacements,
}
